Function Get-OSType{
Param ($ID)
 switch($ID){
0{"Unknown"}
1{"Other"}
2{"MACOS"}
3{"ATTUNIX"}
4{"DGUX"}
5{"DECNT"}
6{"Digital Unix"}
7{"OpenVMS"}
8{"HPUX"}
9{"AIX"}
10{"MVS"}
11{"OS400"}
12{"OS/2"}
13{"JavaVM"}
14{"MSDOS"}
15{"WIN3x"}
16{"WIN95"}
17{"WIN98"}
18{"WINNT"}
19{"WINCE"}
20{"NCR3000"}
21{"NetWare"}
22{"OSF"}
23{"DC/OS"}
24{"Reliant UNIX"}
25{"SCO UnixWare"}
26{"SCO OpenServer"}
27{"Sequent"}
28{"IRIX"}
29{"Solaris"}
30{"SunOS"}
31{"U6000"}
32{"ASERIES"}
33{"TandemNSK"}
34{"TandemNT"}
35{"BS2000"}
36{"LINUX"}
37{"Lynx"}
38{"XENIX"}
39{"VM/ESA"}
40{"Interactive UNIX"}
41{"BSDUNIX"}
42{"FreeBSD"}
43{"NetBSD"}
44{"GNU Hurd"}
45{"OS9"}
46{"MACH Kernel"}
47{"Inferno"}
48{"QNX"}
49{"EPOC"}
50{"IxWorks"}
51{"VxWorks"}
52{"MiNT"}
53{"BeOS"}
54{"HP MPE"}
55{"NextStep"}
56{"PalmPilot"}
57{"Rhapsody"}
58{"Windows"}
59{"Dedicated"}
60{"OS/390"}
61{"VSE"} 
62{"TPF"}
}

}
Function Get-memorysizeinGB{
Param ($size)
return ""+ [math]::round($size /1Gb, 3) +"GB"
}
$ErrorActionPreference = 'Continue'
try{ 
$compSys = Get-WMIObject -class Win32_ComputerSystem | select Manufacturer,SerialNumber,PCSystemType,Model, SystemType
}
catch{
$compSys = New-Object -TypeName PSObject
}
try{
$windowsInfo= Get-WmiObject Win32_OperatingSystem   | select Version,TotalVirtualMemorySize,LocalDateTime,OSType,ServicePackMajorVersion,ServicePackMinorVersion 
$windowsInfo.OSType=Get-OSType($windowsInfo.OSType)
$windowsInfo.TotalVirtualMemorySize=Get-memorysizeinGB($windowsInfo.TotalVirtualMemorySize)
$windowsInfo.LocalDateTime=Get-Date -format g

}
catch{
$windowsInfo = New-Object -TypeName PSObject
}
try{
$processorInfo=Get-WmiObject Win32_Processor  | select Architecture 
}
catch{
$processorInfo = New-Object -TypeName PSObject
}
try{
$hardwareInfo=Get-WmiObject Win32_DiskDrive  | select Name,Size,Partitions 
foreach ($info in $hardwareInfo) {
$info.Size=Get-memorysizeinGB($info.Size)
}
}
catch{
$hardwareInfo = New-Object -TypeName PSObject
}
try{
$massStorageDeviceInfo=Get-WmiObject Win32_LogicalDisk  | select Name,DriveType,Size,FileSystem,VolumeSerialNumber 
foreach ($info in $massStorageDeviceInfo) {
$info.Size=Get-memorysizeinGB($info.Size)
}
}
catch{
$massStorageDeviceInfo = New-Object -TypeName PSObject
}
try{
$sysMemoryInfo=Get-WmiObject Win32_PhysicalMemory  | select MemoryType,Capacity 
foreach ($info in $sysMemoryInfo) {
$info.Capacity=Get-memorysizeinGB($info.Capacity)
}
}
catch{
$sysMemoryInfo = New-Object -TypeName PSObject
}
try{
$printerInfo=Get-WmiObject Win32_Printer  | select Name,PortName,Shared,ShareName 
}
catch{
$printerInfo = New-Object -TypeName PSObject
}
try{
$netAdapterInfo=Get-WmiObject Win32_NetworkAdapterConfiguration  | select Description,MACAddress,DHCPServer,IPAddress,DNSHostName,IPSubnet,DefaultIPGateway 
}
catch{
$netAdapterInfo = New-Object -TypeName PSObject
}
try{
$systemUserInfo=Get-WmiObject -Class Win32_UserAccount -Filter  "LocalAccount='True'"  | select Name, disabled
}
catch{
$systemUserInfo = New-Object -TypeName PSObject
}
try{
$antivirusInfo=Get-WmiObject -namespace root\securitycenter2 -class AntivirusProduct  | select displayName 
if (!$antivirusInfo){
$antivirusInfo = New-Object -TypeName PSObject
Add-Member -InputObject $antivirusInfo -MemberType NoteProperty `
  -Name displayName -Value ""
}
}
catch{
$antivirusInfo = New-Object -TypeName PSObject
Add-Member -InputObject $antivirusInfo -MemberType NoteProperty `
  -Name displayName -Value ""
$ErrorMessage = $_.Exception.Message
Write-Output "Exception in antivirus Info : "$ErrorMessage
}
try{
$envVariables = New-Object -TypeName PSObject
$envVariables=Get-WmiObject Win32_Environment   | SELECT Name, VariableValue 
}
catch{
$envVariables = New-Object -TypeName PSObject
}
try{
$loggedUserInfo = New-Object -TypeName PSObject
$loggedUserInfo=Get-WMIObject -class Win32_ComputerSystem | select username
}
catch{
$loggedUserInfo = New-Object -TypeName PSObject
}
try{
$restoreInfo = New-Object -TypeName PSObject
#$restoreInfo=Get-ComputerRestorePoint | Select Description, CreationTime
if (!$restoreInfo){
$restoreInfo = New-Object -TypeName PSObject
Add-Member -InputObject $restoreInfo -MemberType NoteProperty `
  -Name Description -Value ""
Add-Member -InputObject $restoreInfo -MemberType NoteProperty `
  -Name CreationTime -Value ""
}

}
catch{
$restoreInfo = New-Object -TypeName PSObject
}
try{
$driverInfo = New-Object -TypeName PSObject
$driverInfo=Get-WmiObject Win32_PnPSignedDriver| select devicename, driverversion ,path, location | where devicename -ne $null
}
catch{
$driverInfo = New-Object -TypeName PSObject
}
try{
$mailInfo = New-Object -TypeName PSObject
$mailInfo = Get-ItemProperty -Path Registry::HKEY_CLASSES_ROOT\mailTo\shell\open\command
$MailClient = $mailInfo.'(default)'
if (!$mailInfo) { 
$MailClient = ""
}
}
catch{
$mailInfo = New-Object -TypeName PSObject
$mailInfo ==""
}
try{
$browserInfo = New-Object -TypeName PSObject
$browserInfo = Get-ItemProperty -Path Registry::HKEY_CLASSES_ROOT\http\shell\open\command
$browserInfo = $browserInfo.'(default)'
if (!$browserInfo) { 
$browserInfo = ""
}

}
catch{
$browserInfo = New-Object -TypeName PSObject
$browserInfo=""
}
$inventoryInfo = New-Object -TypeName PSObject

$inventoryInfo | Add-Member -MemberType NoteProperty �Name 'systemVendorInfo' �Value $compSys
$inventoryInfo | Add-Member -MemberType NoteProperty �Name 'windowsInfo' �Value $windowsInfo

$inventoryInfo | Add-Member -MemberType NoteProperty  �Name 'processorInfo' �Value $processorInfo

$inventoryInfo | Add-Member �MemberType NoteProperty �Name 'hardDriveInfo' �Value $hardwareInfo

$inventoryInfo | Add-Member �MemberType NoteProperty �Name 'massStorageDeviceInfo' �Value $massStorageDeviceInfo

$inventoryInfo | Add-Member �MemberType NoteProperty �Name 'sysMemoryInfo' �Value $sysMemoryInfo

$inventoryInfo | Add-Member �MemberType NoteProperty �Name 'printerInfo' �Value $printerInfo

$inventoryInfo | Add-Member �MemberType NoteProperty �Name 'netAdapterInfo' �Value $netAdapterInfo

$inventoryInfo | Add-Member �MemberType NoteProperty �Name 'systemUserInfo' �Value $systemUserInfo

$inventoryInfo | Add-Member -MemberType NoteProperty -Name 'antivirusInfo' -Value $antivirusInfo

$inventoryInfo | Add-Member -MemberType NoteProperty -Name 'restoreInfo' -Value $restoreInfo

$inventoryInfo | Add-Member -MemberType NoteProperty -Name 'loggedUserInfo' -Value $loggedUserInfo

$inventoryInfo | Add-Member -MemberType NoteProperty -Name 'MailClient' -Value $MailClient

$inventoryInfo | Add-Member -MemberType NoteProperty -Name 'browserInfo' -Value $browserInfo

$inventoryInfo | Add-Member -MemberType NoteProperty -Name 'driverInfo' -Value $driverInfo

#$inventoryInfo | Add-Member �MemberType NoteProperty �Name 'envVariables' �Value $envVariables

try{
echo ($inventoryInfo | ConvertTo-JSON )
}
catch{
$ErrorMessage = $_.Exception.Message
	Write-Output "Exception in conversion : "$ErrorMessage
}