Sub GetAdministrators(strComputerName)
    Dim objWMIService, strQuery, colItems, Path, strMembers
    Set objWMIService = GetObject("winmgmts:{impersonationLevel=impersonate}!\\.\root\cimv2")
    strQuery = "select * from Win32_GroupUser where GroupComponent = " & chr(34) & "Win32_Group.Domain='" & strComputerName & "',Name='Administrators'" & Chr(34)
    Set ColItems = objWMIService.ExecQuery(strQuery)
    strMembers = ""
    For Each Path In ColItems
        Dim strMemberName, NamesArray, strDomainName, DomainNameArray
        NamesArray = Split(Path.PartComponent,",")
        strMemberName = Replace(Replace(NamesArray(1),Chr(34),""),"Name=","")
        DomainNameArray = Split(NamesArray(0),"=")
        strDomainName = Replace(DomainNameArray(1),Chr(34),"")
        If strDomainName <> strComputerName Then
            strMemberName = strDomainName & "\" & strMemberName
        End If
        WScript.Echo strMemberName
    Next
End Sub

Function GetComputerName()
    Set objWMISvc = GetObject( "winmgmts:\\.\root\cimv2" )
    Set colItems = objWMISvc.ExecQuery( "Select * from Win32_ComputerSystem", , 48 )
    For Each objItem in colItems
        strComputerName = objItem.Name
        GetComputerName = strComputerName
    Next
End Function

GetAdministrators GetComputerName

