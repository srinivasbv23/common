$proid=$args[0]

Get-WmiObject Win32_Service -FIlter "Pathname like '%svchost%'"|
ForEach-Object{
$p = Get-Process -PID $proid
$p | Add-Member -MemberType NoteProperty -Name ServiceName -Value $_.Caption -PassThru
} |
Sort-Object WS -Descending | Select ServiceName,@{Name="MemUsage(MB)";Expression={[math]::round($_.ws /
1mb)}},@{Name="PID";Expression={[String]$_.ID}} |ConvertTo-Json -Compress 
