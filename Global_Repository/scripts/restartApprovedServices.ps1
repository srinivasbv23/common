<#
workflow
restart approved services
#>

#Function

Function getCurTime {
$ctime=get-date -UFormat "%Y-%m-%d %H:%M:%S"
return $ctime
}


Function restartProcess ($process) {
$proMemUsage=0
$memory = Get-WmiObject win32_computersystem 
$Threshold= $memory.TotalPhysicalMemory * 0.1
get-Process $process| select -Property Id,WS | ? {$_.WS -gt $Threshold} | Stop-process -Force
$curTime=getCurTime
$proMemUsage=(get-process $process|select -property ws)|out-string
$proMemUsage=$proMemUsage/1
if ($proMemUsage -gt $Threshold) {
echo "$process consume more than 10% total memory($proMemUsage) at $curTime ,restarted it. end"
restart -name $process
return 1
}
return 0
}


$ErrorActionPreference = 'SilentlyContinue'
$curTime=getCurTime

#below is the main part of the script.
#
echo "Check approved services. end"

$restartStatus=0
$result=0
$appProcess=@("Netiqccm","netiqmc","besclient","watchdog")
ForEach( $proc  in $appProcess)
{
$result=restartProcess $proc
if ($result -eq 1){
$restartStatus=1
}
}
if ($restartStatus -eq 0) {
echo "No approved service consume more than 10% memory resources at $curTime. end"
}
echo "Completed checking approved services. end"
