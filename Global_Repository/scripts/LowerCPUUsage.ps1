<#
workflow
1: get current cpu usage.
2: if current cpu usage is greater than 95%, if it's Friday, check the top 3 processes and consuming 10% cpu resource.
   if current cpu usage is less than 95%, exit and update ticket with 'CPU spike, current cpu usage:?? no issue found."
3: if MacFee is one of the top 3, restart it.
4: if current cpu usage is greater than 95%, if it's not Friday check the top 5 processes
5: if the approved 4 service is in top 5 and consuming more than 10% cpu. restart them.
6: update the ticket with these information.
powershell:
$OutputVariable = (Shell command) | Out-String
#>

#Function

Function GetTop5Process {
$Get_CpuLoad = {
   $M1 = Get-WmiObject -Query "select * from Win32_PerfRawData_PerfProc_Process"
   Start-Sleep -Seconds 1
   $M2 = Get-WmiObject -Query "select * from Win32_PerfRawData_PerfProc_Process" 
   
   for($i = 0;$i -lt $M1.Count;$i++){
       $Load = [math]::Round((($M2[$i].PercentProcessorTime - $M1[$i].PercentProcessorTime)/($M2[$i].Timestamp_Sys100NS - $M1[$i].Timestamp_Sys100NS)) *100/$env:NUMBER_OF_PROCESSORS,2)
       if($Load -lt 0){$Load = "Idle"}
       
       $CoreInfo = New-Object psobject -Property @{
           ProcessName    = ($M1[$i].name)
           PID            = ($M1[$i].IDProcess)
           ThreadCount    = ($M1[$i].ThreadCount)
           LoadPercentage = $Load
       }
       [array]$result += $CoreInfo
   }
   return $result 
}
$Get_CpuLoad.Invoke() | Sort LoadPercentage -descending | Select -first 7 | Format-Table

}


Function GetTop3Process {
$Get_CpuLoad = {
   $M1 = Get-WmiObject -Query "select * from Win32_PerfRawData_PerfProc_Process"
   Start-Sleep -Seconds 1
   $M2 = Get-WmiObject -Query "select * from Win32_PerfRawData_PerfProc_Process" 
   
   for($i = 0;$i -lt $M1.Count;$i++){
       $Load = [math]::Round((($M2[$i].PercentProcessorTime - $M1[$i].PercentProcessorTime)/($M2[$i].Timestamp_Sys100NS - $M1[$i].Timestamp_Sys100NS)) *100/$env:NUMBER_OF_PROCESSORS,2)
       if($Load -lt 0){$Load = "Idle"}
       
       $CoreInfo = New-Object psobject -Property @{
           ProcessName    = ($M1[$i].name)
           PID            = ($M1[$i].IDProcess)
           ThreadCount    = ($M1[$i].ThreadCount)
           LoadPercentage = $Load
       }
       [array]$result += $CoreInfo
   }
   return $result 
}
$Get_CpuLoad.Invoke() | Sort LoadPercentage -descending | Select -first 5 | Format-Table

}

#Get the current cpu usage:
$Output=(get-counter -Counter "\Processor(_Total)\% Processor Time" -SampleInterval 1)|Out-String
$Output=$Output.trim()
#echo $Output.Split()[-1]
$cpuusage=$Output.Split()[-1].trim()
$cpuusage=$cpuusage/1

if($cpuusage -gt 95)
{

#check if it's Friday today
$a=get-date
if($a.DayOfWeek -eq 'Friday') {

$topProcess3=GetTop3Process
echo $topProcess|Out-File c:\tmp\toprocess.out
$result10=(Get-Content c:\tmp\toprocess.out |Select-String McAfee)|Out-String
$procUsage3=$result10.trim().split()[0]
$procUsage3=$procUsage3/1

if($procUsage3 -gt 10)
{
echo  "McAfee consume $procUsage3 % CPU resource currently!"
echo  "restart McAfee !!"
restart-service -Name McAfee
}

}

#complete handling McAfee checking

$topProcess=GetTop5Process
echo $topProcess |Out-File c:\tmp\toprocess.out
#echo $topProcess
$appProcess=@("chrome","notepad","besclient","360tray","McAfee")
ForEach( $proc  in $appProcess)
{
$result100=(Get-Content c:\tmp\toprocess.out |Select-String $proc)|Out-String
#echo $result100
$proUsage=$result100.trim().split()[0]
#$test=$result100.trim().split()[-1]
#echo "$proUsage $test"
$proUsage=$proUsage/1
#echo "$proUsage 100"
if($proUsage -gt 10)
{
echo "restart $proc because it consume $proUsage %!!"
restart-service -Name $proc
}
}

#check again

$Output=(get-counter -Counter "\Processor(_Total)\% Processor Time" -SampleInterval 1)|Out-String
$Output=$Output.trim()
#echo $Output.Split()[-1]
$cpuusage=$Output.Split()[-1].trim()
$cpuusage=$cpuusage/1

if($cpuusage -gt 95)
{
$topProcess=GetTop5Process
echo "CPUS usage is $cpuusage %, Hiro can not resolve this issue!"
echo "Below is the top 5 processes which consume most of the CPU resource"
echo $topProcess
exit
}else
{
echo "ISSUE has been resolved, current CPU usage is $cpuusage  !!"
}

#End second check

}
else
{
echo "current CPU usage is $cpuusage %, no issue found. resolve the ticket."
}


<#
(1)Netiqccm.exe - NetIQ AppManager Client Communication Manager 
(2) netiqmc.exe - NetIQ AppManager Client Resource Monitor 
(3)watchdog.exe � OpswareAgent  
(4)besclient.exe - BESClient

This will list the processes that was using >5% of CPU at the instance the sample was taken
(Get-Counter '\Process(*)\% Processor Time').CounterSamples | Where-Object {$_.CookedValue -gt 10}

echo $topProcess |Out-File c:\tmp\toprocess.out
Get-Content c:\tmp\toprocess.out |Select-String 'powershell'


Get-Content <file name>.txt | Select-String '(phrase)'
######
if it's Friday today?

$a=get-date
if($a.DayOfWeek -eq 'Friday') 
{ $b = "05" }
$b | out-file c:\day.txt
#>

