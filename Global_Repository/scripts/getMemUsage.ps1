
<#
1: get current mem usage.
#>

#Function

Function getCurTime {
$ctime=get-date -UFormat "%Y-%m-%d %H:%M:%S"
return $ctime
}

Function GetMemUsage {
$memusage=(Get-WmiObject win32_operatingsystem | Select-Object @{Name="Free Memory (%)"; Expression={[decimal]::round((($_.TotalVisibleMemorySize - $_.FreePhysicalMemory)*100)/ $_.TotalVisibleMemorySize, 2)}}|Format-Table -HideTableHeaders)|Out-String
$memusage=[int]$memusage
return $memusage
}


$memUsage=GetMemUsage
$curTime=getCurTime

echo $memUsage at $curTime
