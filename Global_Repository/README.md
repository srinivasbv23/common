# Ansible HIROCTL

###### Module: `hiroctl`

## Description:

Manage HIRO services.
This module will help you start, stop and restart services in HIRO.

### EXAMPLE

```yaml
- name: Make sure a service is running
  hiroctl:
    state: started
    name: engine

- name: stop service graph on centos, if running
  hiroctl:
    name: graph
    state: stopped
```

For usage of this playbook, please refer to the [WIKI](https://bitbucket.org/_miguel_ortiz_/ansible_modules/wiki/Home).