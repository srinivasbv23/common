#!/usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import absolute_import, division, print_function
__metaclass__ = type

ANSIBLE_METADATA = {'metadata_version': '1.1', 
                     'status': ['preview'],
                    'supported_by': 'community'}

import smtplib
from smtplib import SMTPException
from email.MIMEMultipart import MIMEMultipart
from email.mime.application import MIMEApplication
from email.MIMEBase import MIMEBase
from email import Encoders
from email.mime.text import MIMEText
import os
import sys
import json
import re
import time
import datetime
import commands


from ansible.module_utils.basic import AnsibleModule

def sendEmail(subj,eText,recipient):
  subject = subj
  #print(subj)
  me = "ansiblereport@gmail.com"
  toaddr = recipient

  msg = MIMEText(eText)
  msg['Subject'] = subject
  msg['From'] = me
  msg['To'] = toaddr


  try:
    s = smtplib.SMTP('smtp.gmail.com', 587)
    s.ehlo()
    s.starttls()
    s.ehlo()
    s.login(user = 'ansiblereport@gmail.com', password = '!Wwsz.7559')
    #s.sendmail(me, str(toaddr).split(","), msg.as_string())
    s.sendmail(me,toaddr,msg.as_string())
    s.quit()
    #print("Email has been sent to " + str(toaddr))
    result="Email has been send to " + str(toaddr)
    return(result)
  except SMTPException as error:
    return(error)



def main():
  module = AnsibleModule(
    argument_spec=dict(
      emailto=dict(type='str', required=True),
      noteinfo=dict(type='str', required=True),
      subject=dict(type='str', required=True)),
      supports_check_mode=False,
    )

  result = {
    'name': module.params['subject'],
    'changed': False,
    'msg': ""
    }


  emailto = module.params['emailto']
  noteinfo = module.params['noteinfo']
  subject = module.params['subject']
  out=''
  out=sendEmail(subject,noteinfo,emailto)
  result["msg"]=out
  result["changed"]=False

  module.exit_json(**result)
    
if __name__ == '__main__':
  main()
