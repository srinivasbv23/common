#!/usr/bin/python
# -*- coding: utf-8 -*-

#

from __future__ import absolute_import, division, print_function
__metaclass__ = type

ANSIBLE_METADATA = {'metadata_version': '1.1',
                    'status': ['stableinterface'],
                    'supported_by': 'core'}

import glob
import os
import platform
import re
import select
import shlex
import string
import subprocess
import tempfile
import time


from ansible.module_utils.basic import AnsibleModule, load_platform_subclass
from ansible.module_utils.service import fail_if_missing
from ansible.module_utils.six import PY2, b
from ansible.module_utils._text import to_bytes, to_text

def getCoreDumpInfo(module):
  coreFile="/data/oracle/EBS_PROD/inst/apps/EBSPROD_cysbigdcapmdw13/admin/scripts/core"
  progName="FNDFS"
  result={}
  result={"coreFile":coreFile,"progName":progName}
  return(result)


def getPagingSpaceUsage(module):
  totalPSpace="1024MB"
  percentageOfPSpace="30"
  aboveThreshold="False"
  result={}
  result={"totalPSpace":totalPSpace,"percentageOfPSpace":percentageOfPSpace,"aboveThreshold":aboveThreshold}
  return(result)  

def main():
  module = AnsibleModule(
    argument_spec=dict(
      errorID=dict(type='str', required=True , choices=['A924A5FC', 'C5C09FFA'])),
      supports_check_mode=False,
    )

  result = {
    'name': module.params['errorID'],
    'changed': False,
    'msg': "",
    'timestamp': ""
    }

  errorID = module.params['errorID']

  date_cmd=module.get_bin_path("date", required=True)
  extraFlag=" +'%F %H:%M:%S'"
  rc=timestamp=err=''
  (rc, timestamp, err) = module.run_command("%s  %s" % (date_cmd, extraFlag))
  result['timestamp']=timestamp

  if "A924A5FC" in errorID:
    resultCoreDumpInfo=getCoreDumpInfo(module)
    result['changed']=False
    result['msg']=resultCoreDumpInfo
  elif "C5C09FFA" in errorID:
    resultPagingSpaceUsage=getPagingSpaceUsage(module)
    result['changed']=False
    result['msg']=resultPagingSpaceUsage

  module.exit_json(**result)


if __name__ == '__main__':
  main()
