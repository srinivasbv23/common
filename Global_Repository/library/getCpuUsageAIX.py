#!/usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import absolute_import, division, print_function
__metaclass__ = type

ANSIBLE_METADATA = {'metadata_version': '1.1', 
                     'status': ['preview'],
                    'supported_by': 'community'}

import re
import os
import sys
import time
import csv
from operator import itemgetter

from ansible.module_utils.basic import AnsibleModule

def parse_cpuusage_aix(cpuUsage_output):
  file=open("/tmp/test2.out", "wb+")
  file.write(cpuUsage_output)
  file.close()
  lines = list()
  with open("/tmp/test2.out") as f:
    for line in f:
      line=re.sub(' +', ' ', line)
      #print(line)
      lines.append(line.strip())

  flag1=False
  result="nodata"
  for line in lines:
    if "Average" in line:
      flag1=True
    if flag1==True and ( 'all' in line or '-' in line):
      sp=[]
      sp=line.split(' ')
      usr=sp[2]
      sys=sp[4]
      cpuUsage=float(usr)+float(sys)
      #print(str(cpuUsage))
      result=str(cpuUsage)
      break
  return(result)


def parse_top5proc_aix(cpuTop5Proc_output):
  file=open("/tmp/test.out","wb+")
  file.write(cpuTop5Proc_output)
  file.close()
  lines = list()
  with open("/tmp/test.out") as f:
    for line in f:
      line=re.sub(' +', ' ', line)
      #print(line)
      lines.append(line.strip())
  num=0
  result=[]
  for line in sorted(lines, key=lambda line: line.split()[1],reverse=True):
    if num<5:
      #print(line)
      sp=[]
      sp=line.split(' ')
      user=sp[0]
      pcpu=sp[1]
      pid=sp[2]
      time=sp[3]
      cmd=sp[4]
      try:
        if sp[5]:
          cmd=sp[4]+' '+sp[5]
      except:
        pass
      b={}
      b={"user":user,"pcpu":pcpu,"pid":pid,"time":time,"cmd":cmd}
      result.append(dict(b))
      #result += user+' , '+pcpu+' , '+pid+' , '+time+' , '+cmd+'\n'
    else:
      break
    num += 1
  return(result)


def getTop5Proc_aix(module):
  ps_cmd = module.get_bin_path("ps", required=True)
  outputFormat1="user,pcpu,pid,time,cmd" 
  rc=err=cpuTop5Proc_output=''
  rc, cpuTop5Proc_output, err = module.run_command("%s -Ao %s" % (ps_cmd,outputFormat1 ))
  if rc==0:
    return(cpuTop5Proc_output)
  else:
    return("Failed to collect cpu usage data.")

def getCPUUsage_aix(module):
  sar_cmd = module.get_bin_path("sar" , required=True)
  extraFlags="ALL 4 2"
  rc=err=cpuUsage_output=""
  rc, cpuUsage_output, err = module.run_command("%s -P %s" %(sar_cmd, extraFlags))
  if rc==0:
    return(cpuUsage_output)
  else:
    return("Failed to collect top 5 cpu offenders.")



def main():
  module = AnsibleModule(
    argument_spec=dict(
      action=dict(type='str', required=True , choices=['GetTotalCPUUsage', 'GetTop5CpuConsumer'])),
      supports_check_mode=False,
    )

  result = {
        'name': module.params['action'],
        'changed': False,
        'msg': ""
    }

  operName = module.params['action']
  
  if "GetTotalCPUUsage" in operName:
    result1=getCPUUsage_aix(module)
    resultCpuUsage=parse_cpuusage_aix(result1)
    result['changed']=False
    result['msg']=resultCpuUsage
  elif "GetTop5CpuConsumer" in operName:
    result2=getTop5Proc_aix(module)
    resultTop5Proc=parse_top5proc_aix(result2)
    result['changed']=False
    result['msg']=resultTop5Proc    
  
  module.exit_json(**result)

  #if rc1 == 0:
    #module.exit_json(changed=True, msg="File system name is: %s ." % fsName)
  #else:
    #module.fail_json(msg="File system: %s ." % fsName)
    
if __name__ == '__main__':
  main()  
