#Para correrlo: <scriptname>.ps1 -packagename WinRar -silentArgs /S
param(
  [string]$packageName,
  [string]$silentArgs)

. C:\ProgramData\chocolatey\helpers\functions\Uninstall-ChocolateyPackage.ps1
. C:\ProgramData\chocolatey\helpers\functions\Start-ChocolateyProcessAsAdmin.ps1

#$packageName = 'WinRAR'
$packageSearch = "$packageName*"
$installerType = 'exe'
#$silentArgs = '/S'
$validExitCodes = @(0)

echo "Searching for package: $packageName using silentArgs: $silentArgs" 

Get-ItemProperty -Path @('HKLM:\Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\*',
                         'HKLM:\Software\Microsoft\Windows\CurrentVersion\Uninstall\*',
                         'HKCU:\Software\Microsoft\Windows\CurrentVersion\Uninstall\*') `
                 -ErrorAction:SilentlyContinue `
| Where-Object   {$_.DisplayName -like $packageSearch} `
| ForEach-Object {  echo "Package founded: $($_.DisplayName)"
                    Uninstall-ChocolateyPackage -PackageName "$($_.DisplayName)" `
                                              -FileType "$installerType" `
                                              -SilentArgs "$($silentArgs)" `
                                              -File "$($_.UninstallString)" `
                                              -ValidExitCodes $validExitCodes}