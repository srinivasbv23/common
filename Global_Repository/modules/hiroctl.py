#!/usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import absolute_import, division, print_function
__metaclass__ = type


DOCUMENTATION = '''
module: hiroctl
author:
    - Compucom Development Team
version_added: "2.5"
short_description:  Manage HIRO services
description:
    - Module to start, stop and restart services in HIRO.
options:
    name:
        description:
            - Name of the service.
        aliases: [ service, unit ]
    state:
        description:
            - C(started)/C(stopped) are idempotent actions that will not run commands unless necessary.
              C(restart) will always bounce the service.
        choices: [ restart, started, stopped ]
requirements:
    - HIRO installation should always be in the default path with your admin commands at: /opt/autopilot/admin 
'''

EXAMPLES = '''
- name: Make sure a service is running
  hiroctl:
    state: started
    name: engine

- name: stop service graph on centos, if running
  hiroctl:
    name: graph
    state: stopped
'''

from ansible.module_utils.basic import AnsibleModule
from ansible.module_utils.service import sysv_exists, sysv_is_enabled, fail_if_missing
from ansible.module_utils._text import to_native

def request_was_ignored(out):
    return '=' not in out and 'ignoring request' in out


def parse_hiroctl_show(lines,unit):
    parsed = {}
    multival = []
    for line in lines:
        if '-' in line and unit in line:
            k, v = line.split('-', 1)
            multival.append(v)
            parsed[k.strip()] = v.strip()
    return parsed


# ===========================================
# Main control flow

def main():
    # initialize
    module = AnsibleModule(
        argument_spec=dict(
            name=dict(type='str', aliases=['service', 'unit']),
            state=dict(type='str', choices=['restart', 'started', 'stopped']),
        ),
        supports_check_mode=True,
        required_one_of=[['state', 'enabled']],
    )

    hiroctl = '/opt/autopilot/admin/'
    requires_service = False
    # To be included: ,'connect-change-consumer','connect-change-rest-api','connect-config-consumer','connect-config-rest-api','connect-event-consumer','connect-event-rest-api','connect-incident-backsync','connect-incident-consumer','connect-incident-rest-api'
    # To be added: result.update(self._execute_module(module_name='service', module_args=new_module_args, task_vars=task_vars, tmp=tmp, delete_remote_tmp=False))
    list_of_services = ['all','zookeeper','kafka','cassandra','wso2is','hadoop','graphit','engine','engine_proxy','connect']
    unit = module.params['name']
    rc = 0
    out = err = ''
    result = dict(
        name=unit,
        changed=False,
        status=dict(),
    )
    # Does service exist?
    if unit not in list_of_services:
        module.fail_json(msg="value %s doesn't exist, please try with one of the following values: %s" % (unit, str(list_of_services)))

    if unit == 'all':
        unit == '-a'

    if unit.startswith('connect-') and module.params['state'] == 'started':
        requires_service = True

    for requires in ['state']:
        if module.params[requires] is not None and unit is None:
            module.fail_json(msg="name is also required when specifying %s" % requires)

    if unit:
        found = False
        is_initd = sysv_exists(unit)
        is_hiroctl = False
        is_running_service = False

        # check if service is currently running
        (rc, out, err) = module.run_command("%sstatus-autopilot.sh '%s'" % (hiroctl, unit))

        if rc == 0:
            # load in a dictionary values with the 'unit' tag
            if out:
              # Parsing feature is pending to be included
              #  result['status'] = parse_hiroctl_show(to_native(out).split('\n'),unit)
                result['status'] = out

                is_running_service = [is_running for is_running in result['status'] if 'IU' in is_running]

                if len(is_running_service) == 0:
                    is_running_service = False

        # set service state if requested
        if module.params['state'] is not None:
#            fail_if_missing(module, found, unit, msg="host")

            # default to desired state
            result['state'] = module.params['state']

            # What is current service state?
            if 'IU' in result['status']:
                action = None
                if module.params['state'] == 'started':
                    if is_running_service:
                        action = 'start-autopilot.sh'
                elif module.params['state'] == 'stopped':
                    if not is_running_service:
                        action = 'stop-autopilot.sh'
                elif module.params['state'] == 'restart':
                        action = 'start-autopilot.sh -r'

                if action:
                    result['changed'] = True
                    if not module.check_mode:
                        (rc, out, err) = module.run_command("%s%s '%s'" % (hiroctl, action, unit))
                        if rc != 0:
                            module.fail_json(msg="Unable to %s service %s: %s" % (action, unit, err))
            else:
                # this should not happen?
                module.fail_json(msg="Service is in unknown state", status=result['status'])

    module.exit_json(**result)


if __name__ == '__main__':
    main()
