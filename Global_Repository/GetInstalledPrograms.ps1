$Computer= $env:COMPUTERNAME
$RegistryLocation = 'SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall',
                    'SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall'
$HashProperty = @{}
$Property = @('DisplayName','UninstallString','Version','InstallDate','InstallLocation')
$SelectProperty = @('DisplayName','UninstallString','Version','InstallDate','InstallLocation')
$RegBase = [Microsoft.Win32.RegistryKey]::OpenRemoteBaseKey('LocalMachine',$Computer)
foreach ($CurrentReg in $RegistryLocation) 
{
    if ($RegBase) 
    {
        
        $CurrentRegKey = $RegBase.OpenSubKey($CurrentReg)
        if ($CurrentRegKey) 
        {
            
            $CurrentRegKey.GetSubKeyNames() | ForEach-Object{
                
                if ($Property) 
                {
                    foreach ($CurrentProperty in $Property) 
                    {
                        $HashProperty.$CurrentProperty = ($CurrentRegKey.OpenSubKey($_)).GetValue($CurrentProperty)
                    }
                }
                $HashProperty.ComputerName = $Computer
                $HashProperty.ProgramName = ($DisplayName = ($CurrentRegKey.OpenSubKey($_)).GetValue('DisplayName'))
                if ($DisplayName) 
                {
                    New-Object -TypeName PSCustomObject -Property $HashProperty |
                    Select-Object -Property $SelectProperty
                } 
            }
        }
    }
}
