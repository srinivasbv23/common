﻿[CmdletBinding()]

Param(
    [Parameter(Mandatory=$true)]
	[string]$drive
	)
	
echo  $drive

$verboseDefragOutput = ( &{Optimize-Volume -DriveLetter $drive -Defrag  -Verbose} 4>&1 )
