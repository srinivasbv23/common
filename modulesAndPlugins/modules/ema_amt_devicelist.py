import json
import urllib2
import ssl
import urllib
import requests
import ema_token_auth
from urllib2 import urlopen, URLError, HTTPError
from socket import timeout


class UrlDeviceListResp(ema_token_auth.EmaTokenResp):

    def getDeviceList(self, server, token):

        str_token = 'Bearer ' + str(token['msg'])
        devicelist_headers = {'Authorization': str_token}

        try:
            respdevicelist = UrlDeviceListResp.s.get(server, headers=devicelist_headers)
            if respdevicelist.status_code == 200:
             respText = self.onSuccess(respdevicelist)
            elif respdevicelist.status_code == 404:
             print respText
        except HTTPError as e:
            print("HTTP error block")
            respText = self.onError(respdevicelist, e)
        except URLError as e:
            print("URL error block")
            respText = self.onError(respdevicelist, e)
        except timeout as e:
            print("timeout error block")
            respText = "Request timed out!"
        except KeyError as e:
            print("key error block")
            respText = self.onError(respdevicelist, e)
        except NameError as e:
            print("name error block")
            respText = self.onError(respdevicelist, e)

        return respText

    def onError(self, resp, *args):
        reason = ""
        respText = ""

        if args != None:
            if type(args[0]) is URLError: reason = args[0].reason
        else:
            reason = "{0}; Code: {1}".format(args[0].reason, args[0].code)
        if (resp == None):
            respText =  "Error: {0}".format(reason)
        else:
            respText =  "Error: " + "No device(s) found with the GroupId"

        return respText

    def onSuccess(self, resp):
        return resp.content