import json
import urllib2
import ssl
import urllib
import requests
import ema_token_auth
from urllib2 import urlopen, URLError, HTTPError
from socket import timeout


class UrlPowerOffOperationResp(ema_token_auth.EmaTokenResp):

    def postPowerOffOperation(self, server, ema_amt_power_off_operation,token):

        str_token = 'Bearer ' + str(token['msg'])
        powerOffOperation_headers = {'Authorization': str_token}

        try:
            respPowerOffOperation = UrlPowerOffOperationResp.s.post(server, json=ema_amt_power_off_operation, headers=powerOffOperation_headers)
            respText = self.onSuccess(respPowerOffOperation)
            print respText
        except HTTPError as e:
            print("HTTP error block")
            respText = self.onError(respPowerOffOperation, e)
        except URLError as e:
            print("URL error block")
            respText = self.onError(respPowerOffOperation, e)
        except timeout as e:
            print("timeout error block")
            respText = "Request timed out!"
        except KeyError as e:
            print("key error block")
            respText = self.onError(respPowerOffOperation, e)
        except NameError as e:
            print("name error block")
            respText = self.onError(respPowerOffOperation, e)

        return respText

    def onError(self, resp, *args):
        reason = ""
        respText = ""

        if args != None:
            if type(args[0]) is URLError: reason = args[0].reason
        else:
            reason = "{0}; Code: {1}".format(args[0].reason, args[0].code)
        if (resp == None):
            respText =  "Error: {0}".format(reason)
        else:
            respText =  "Error: " + "Invalid Request"

        return respText

    def onSuccess(self, resp):
        return resp.content
