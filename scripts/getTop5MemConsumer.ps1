
<#
workflow
Get top 5 memory consuming processes.
#>

#Function

Function getCurTime {
$ctime=get-date -UFormat "%Y-%m-%d %H:%M:%S"
return $ctime
}


Function GetTop5Process {
get-wmiobject WIN32_PROCESS | Sort-Object -Property ws -Descending|select -first 8|Select processname, @{Name="MemUsage(MB)";Expression={[math]::round($_.ws /
1mb)}},@{Name="ProcessID";Expression={[String]$_.ProcessID}}|out-file c:\Windows\Temp\top5mem.txt


get-wmiobject WIN32_PROCESS | Sort-Object -Property ws -Descending|select -first 8|Select processname, @{Name="MemUsage(MB)";Expression={[math]::round($_.ws /
1mb)}},@{Name="ProcessID";Expression={[String]$_.ProcessID}}|ConvertTo-Json -Compress|out-file c:\Windows\Temp\top5memV2.txt


$curTime=getCurTime
echo "Below is the top 5 memory consuming processes at $curTime : end"
cat c:\Windows\Temp\top5memV2.txt
}


GetTop5Process
