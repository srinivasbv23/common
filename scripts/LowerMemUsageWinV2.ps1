
<#
workflow
1: get current mem usage.
2: if current mem usage is greater than 95%, check the approved process memory usage.
   if current mem usage is less than 95%, close ticket with 'Memory spike' notes. May check
   5 times, polling interval is 1 second. get the average memory usage.
3: if the following approved 4 service is consuming more than 10% memory. restart them.
   NetIQccm.exe netiqmc.exe watchdog.exe besclient.exe
4: Get top 5 high memory consuming processes.If top 5 contains svchost.exe , get its services.
   update the ticket.
5: function: getTop5Process, restartService, GetMemUsage
powershell:
$OutputVariable = (Shell command) | Out-String
#>

#Function

Function getCurTime {
$ctime=get-date -UFormat "%Y-%m-%d %H:%M:%S"
return $ctime
}

Function GetMemUsage {
$memusage=(Get-WmiObject win32_operatingsystem | Select-Object @{Name="Free Memory (%)"; Expression={[decimal]::round((($_.TotalVisibleMemorySize - $_.FreePhysicalMemory)*100)/ $_.TotalVisibleMemorySize, 2)}}|Format-Table -HideTableHeaders)|Out-String
$memusage=$memusage/1
return $memusage
}

Function restartProcess ($process) {
$proMemUsage=0
$memory = Get-WmiObject win32_computersystem 
$Threshold= $memory.TotalPhysicalMemory * 0.1
get-Process $process| select -Property Id,WS | ? {$_.WS -gt $Threshold} | Stop-process -Force
$curTime=getCurTime
$proMemUsage=(get-process $process|select -property ws)|out-string
$proMemUsage=$proMemUsage/1
if ($proMemUsage -gt $Threshold) {
echo "$process consume more than 10% total memory($proMemUsage), $curTime ,restart it. end"
restart -name $process
return 1
}
return 0
}

Function GetTop5Process {
get-wmiobject WIN32_PROCESS | Sort-Object -Property ws -Descending|select -first 8|Select processname, @{Name="MemUsage(MB)";Expression={[math]::round($_.ws /
1mb)}},@{Name="ProcessID";Expression={[String]$_.ProcessID}}|out-file c:\Windows\Temp\top5mem.txt


get-wmiobject WIN32_PROCESS | Sort-Object -Property ws -Descending|select -first 8|Select processname, @{Name="MemUsage(MB)";Expression={[math]::round($_.ws /
1mb)}},@{Name="ProcessID";Expression={[String]$_.ProcessID}}|ConvertTo-Json -Compress|out-file c:\Windows\Temp\top5memV2.txt


$curTime=getCurTime
echo "Below is the top 5 memory consuming processes at $curTime : end"
cat c:\Windows\Temp\top5memV2.txt

$result=Get-Content c:\Windows\Temp\top5mem.txt|select-string svchost

if ($result -ne $null){

for($i = 0; $i -le $result.count-1; $i++) {
$rStr=$result[$i][0]|out-string
$xpid=$rStr.trim().split()[-1]
#write-host ""
#echo $xpid
tasklist /svc /fi "Pid eq $xpid"|ConvertTo-Json -Compress|out-file c:\Windows\Temp\task.txt
#cat c:\Windows\Temp\task.txt
#write-host ""

Get-WmiObject Win32_Service -FIlter "Pathname like '%svchost%'"|
ForEach-Object{
$p = Get-Process -PID $xpid
$p | Add-Member -MemberType NoteProperty -Name ServiceName -Value $_.Caption -PassThru
} |
Sort-Object WS -Descending | Select ServiceName,@{Name="MemUsage(MB)";Expression={[math]::round($_.ws /
1mb)}},@{Name="PID";Expression={[String]$_.ID}}|ConvertTo-Json -Compress|out-file c:\Windows\Temp\svc.txt
#cat c:\Windows\Temp\svc.txt
}
#end found svchost in top 5 memory consuming process.

} else {
echo "no svchost found in top 5 memory consuming process ! end"
}


}


#GetTop5Process

#command to get the process in svchost
#TASKLIST /scv /fi Pid eq 248  (Pid 248 is an example)
$ErrorActionPreference = 'SilentlyContinue'
$curTime=getCurTime
$memUsage=GetMemUsage
#below is the main part of the script.
$mThreshold=20
if ( $memUsage -gt $mThreshold ) {
echo "Memory usage $memUsage cross threshold $mThreshold % at $curTime!end"

$appProcess=@("Netiqccm","netiqmc","besclient","watchdog")
$sStatus=0
ForEach( $proc  in $appProcess)
{
$result=restartProcess $proc
if ( $result -eq 1 ) {
$sStatus=1
}
}
#if any service get restarted, check memory usage again.
if ( $sStatus -eq 1 ) {

$memUsage=GetMemUsage

if ( $memUsage -gt $mThreshold ) {
echo "Memory usage $memUsage cross threshold $mThreshold % at $curTime!end"
#top 5 process
GetTop5Process
#top 5
}
else {
echo "Memory usage is $memUsage at $curTime,threshold is $mThreshold, No issue found, resolve the issue !end"
exit
}
}
#end memory double check
GetTop5Process
} 
else {
echo "Memory usage is $memUsage at $curTime, No issue found, resolve the issue !end"
exit
}

