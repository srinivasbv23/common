<#

.SYNOPSIS

Pull Printer Job information from Windows Server.

.DESCRIPTION

1 - Return a compressed JSON with the current Printer Jobs queue number.
2 - Return a compressed JSON with the current Printer Jobs for one specific printer.

.PARAMETER checkalljobs

Switch parameter to return number of printer jobs.

.PARAMETER checkprinter

String to pull print jobs for an specific Printer Name.

.EXAMPLE

.\PrinterQcV1.1.ps1 -checkalljobs
{"Name":"_Total","Jobs":0}

.EXAMPLE

.\PrinterQV1.1.ps1 -checkprinter "Fax"
{"Name":"Fax","Jobs":0}

.NOTES

###############################################################################
# Compucom
# FileName: PrinterQcV1.1.ps1
# Version 1.1 / 10/31/2018 08:49:06
# Juan Abundis / juan.abundis@compucom.com
# Revision List:
# 10/30/2018 09:05:38 - Initial Version
# 10/31/2018 08:49:15 - Added option for an specific printer name
# Tested on:
# OS                                           Version 
# --                                           ------- 
# Microsoft Windows Server 2008 R2 Datacenter  6.1.7601
# Microsoft Windows Server 2012 R2 Standard 6.3.9600
# Microsoft Windows 10 Pro 10.0.17134
###############################################################################

.LINK


#>
[CmdletBinding()]
Param(
  [Parameter(Mandatory=$False,ParameterSetName = "Set 1")][ValidateNotNullOrEmpty()]
  [switch]$checkalljobs,
  [Parameter(Mandatory=$False,ParameterSetName = "Set 1")][ValidateNotNullOrEmpty()]
  [string]$checkprinter
)
function GetServerPrintJobs {
    $PrinterJobs = Get-WmiObject -Class Win32_PerfFormattedData_Spooler_PrintQueue | Where-Object -Property Name -Like "_Total" | Select-Object -Property Name,Jobs | ConvertTo-Json -Compress
    return $PrinterJobs
}
function GetPrinterJobs([string]$PrinterName) {
  $PrinterJobs = Get-WmiObject -Class Win32_PerfFormattedData_Spooler_PrintQueue | Where-Object -Property Name -Like $PrinterName | Select-Object -Property Name,Jobs | ConvertTo-Json -Compress
  return $PrinterJobs
}

if ($checkalljobs) {
  GetServerPrintJobs
}

if ($checkprinter) {
  GetPrinterJobs $checkprinter
}
