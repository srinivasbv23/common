<#



.SYNOPSIS

Pending Information (Test)



.DESCRIPTION

Pending Information (Test)



.EXAMPLE

Pending Information (Test)



.NOTES

Pending Information (Test)



.LINK

Pending Information (Test)



#>

[CmdletBinding()]

Param(

  [Parameter(Mandatory=$False,Position=1)][ValidateNotNullOrEmpty()]

  [string]$ExcludeServices,

  [switch]$List

)

Function Get-AutoServNotRun{

  $AutoServNotRun = Get-Service | Select-Object -Property Name,StartType,Status | Sort-Object -Property Status,Name | `

  Where-Object -FilterScript {($_.StartType -eq "Automatic")<# -and ($_.Status -eq "Stopped")#>}

  if(!$AutoServNotRun){

    Out-Default -InputObject "No Automatic/Stopped services found1."

  }elseif($List){

    "Listing Automatic/Stopped services " + (Get-Date -UFormat "%m/%d/%Y %T") | Out-Default

    $AutoServNotRun | Out-Default

  }else{

    "Restart All " + (Get-Date -UFormat "%m/%d/%Y %T") | Out-Default

    $AutoServNotRun | Out-Default

    $AutoServNotRun | Where-Object -FilterScript {($_.Status -eq "Stopped")} | Where-Object -FilterScript {($_.Status -eq "Stopped")} | Stop-Service -Verbose -Force -ErrorAction SilentlyContinue

    Start-Sleep -Seconds 5

    $AutoServNotRun | Where-Object -FilterScript {($_.Status -eq "Stopped")} | Start-Service -Verbose -ErrorAction SilentlyContinue

    Start-Sleep -Seconds 15

    "Final Status " + (Get-Date -UFormat "%m/%d/%Y %T") | Out-Default

    $AutoServNotRun | Get-Service | Select-Object -Property Name,StartType,Status | Sort-Object -Property Status,Name | Out-Default

  }

}

Function Get-AutoServNotRunEx{

  $AutoServNotRunEx = Get-Service | Select-Object -Property Name,StartType,Status | Sort-Object -Property Status,Name | `

  Where-Object -FilterScript {(!$ExcludeServices.Contains($_.Name)) -and ($_.StartType -eq "Automatic") -and ($_.Status -eq "Stopped")}

  if(!$AutoServNotRunEx){

    Out-Default -InputObject "No Automatic/Stopped services found2."

  }else{

    "Restart with Exclusions " + (Get-Date -UFormat "%m/%d/%Y %T") | Out-Default

    $AutoServNotRunEx | Out-Default

    $AutoServNotRunEx | Where-Object -FilterScript {($_.Status -eq "Stopped")} | Stop-Service -Verbose -Force -ErrorAction SilentlyContinue

    Start-Sleep -Seconds 5

    $AutoServNotRunEx | Where-Object -FilterScript {($_.Status -eq "Stopped")} | Start-Service -Verbose -ErrorAction SilentlyContinue

    Start-Sleep -Seconds 15

    "Final Status " + (Get-Date -UFormat "%m/%d/%Y %T") | Out-Default

    $AutoServNotRunEx | Get-Service | Select-Object -Property Name,StartType,Status | Sort-Object -Property Status,Name | Out-Default

  }

}

If($ExcludeServices){

 Get-AutoServNotRunEx

}else{

 Get-AutoServNotRun

}
