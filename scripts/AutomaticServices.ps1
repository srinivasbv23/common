<#



.SYNOPSIS

Pending Information (Test)



.DESCRIPTION

Pending Information (Test)



.EXAMPLE

Pending Information (Test)



.NOTES

Pending Information (Test)



.LINK

Pending Information (Test)
#>


[CmdletBinding()]

Param(

  [Parameter(Mandatory=$False,Position=1)][ValidateNotNullOrEmpty()]

  [string]$ExcludeServices,

  [switch]$List

)

<#

if($args[0]){
  echo $args[0]
  if($args[1]){
    echo $args[1]
    }
}else{
  echo "no argument found"
  exit
}

if($args[1] -and $args[0] -like "*ExcludeService*")
{
  $ExcludeServices=$args[1]
  #echo  $ExcludeServices
}elseif($args[0] -like "*List*") {
  $List="True"
  #echo $List
}elseif(!$args[0]){
  echo "Restarting all services!!"
}elseif($args[0] -like "*ExcludeServices*" and !$args[1]){
  $ExcluedServices=""
}eles{
  echo "exit, no proper argument found!!"
  exit
}
#>

Function Get-AutoServNotRun{

  $AutoServNotRun = Get-Service | Select-Object -Property Name,StartType,Status | Sort-Object -Property Status,Name | `  Where-Object -FilterScript {($_.StartType -eq "Automatic")<# -and ($_.Status -eq "Stopped")#>}|ConvertTo-Json -Compress|Out-String
  if(!$AutoServNotRun){
    Out-Default -InputObject "No Automatic/Stopped services found1."
  }elseif($List){

    $result1="Listing Automatic/Stopped services " + (Get-Date -UFormat "%m/%d/%Y %T")|Out-String 
    echo $result1

    $result2=$AutoServNotRun 
    echo $result2

  }else{

    $result1="Restart All " + (Get-Date -UFormat "%m/%d/%Y %T")|Out-String 
    echo $result1

    $result2=$AutoServNotRun|Out-String
    echo $result2

    $AutoServNotRun | Where-Object -FilterScript {($_.Status -eq "Stopped")} | Where-Object -FilterScript {($_.Status -eq "Stopped")} | Stop-Service -Verbose -Force -ErrorAction SilentlyContinue

    Start-Sleep -Seconds 5

    $AutoServNotRun | Where-Object -FilterScript {($_.Status -eq "Stopped")} | Start-Service -Verbose -ErrorAction SilentlyContinue

    Start-Sleep -Seconds 15

    $result2="Final Status " + (Get-Date -UFormat "%m/%d/%Y %T")|Out-String
    echo $result2

    $result3=$AutoServNotRun | Get-Service | Select-Object -Property Name,StartType,Status | Sort-Object -Property Status,Name |ConvertTo-Json -Compress|Out-String
    echo $result3

  }

}

Function Get-AutoServNotRunEx{

  $AutoServNotRunEx = Get-Service | Select-Object -Property Name,StartType,Status | Sort-Object -Property Status,Name | ` Where-Object -FilterScript {(!$ExcludeServices.Contains($_.Name)) -and ($_.StartType -eq "Automatic") -and ($_.Status -eq "Stopped")}|ConvertTo-Json -Compress|Out-String

  if(!$AutoServNotRunEx){

    Out-Default -InputObject "No Automatic/Stopped services found2."
    echo  "No Automatic/Stopped services found."

  }else{

    $result="Restart with Exclusions " + (Get-Date -UFormat "%m/%d/%Y %T")|Out-String 
    echo $result 

    $result2=$AutoServNotRunEx
    echo $result2

    $AutoServNotRunEx | Where-Object -FilterScript {($_.Status -eq "Stopped")} | Stop-Service -Verbose -Force -ErrorAction SilentlyContinue

    Start-Sleep -Seconds 5

    $AutoServNotRunEx | Where-Object -FilterScript {($_.Status -eq "Stopped")} | Start-Service -Verbose -ErrorAction SilentlyContinue

    Start-Sleep -Seconds 15

    $result2="Final Status " + (Get-Date -UFormat "%m/%d/%Y %T") |Out-String 
    echo $result2

    $result3=$AutoServNotRunEx | Get-Service | Select-Object -Property Name,StartType,Status | Sort-Object -Property Status,Name |ConvertTo-Json -Compress|Out-String 
    echo $result3

  }

}

If($ExcludeServices){
 Get-AutoServNotRunEx
}else{
 Get-AutoServNotRun
}
