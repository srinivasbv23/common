echo timestamp `date +'%Y-%m-%d %H:%M:%S'` ","
ps -e -o pmem,pid,etime,comm | sort -rnk 1,1 | awk '{print $0","}' | head -6
