
#Function

Function GetTop5Process {
$Get_CpuLoad = {
   $M1 = Get-WmiObject -Query "select * from Win32_PerfRawData_PerfProc_Process"
   Start-Sleep -Seconds 1
   $M2 = Get-WmiObject -Query "select * from Win32_PerfRawData_PerfProc_Process" 
   
   for($i = 0;$i -lt $M1.Count;$i++){
       $Load = [math]::Round((($M2[$i].PercentProcessorTime - $M1[$i].PercentProcessorTime)/($M2[$i].Timestamp_Sys100NS - $M1[$i].Timestamp_Sys100NS)) *100/$env:NUMBER_OF_PROCESSORS,2)
       if($Load -lt 0){$Load = "Idle"}
       
       $CoreInfo = New-Object psobject -Property @{
           ProcessName    = ($M1[$i].name)
           PID            = ($M1[$i].IDProcess)
           ThreadCount    = ($M1[$i].ThreadCount)
           LoadPercentage = $Load
       }
       [array]$result += $CoreInfo
   }
   return $result 
}
$Get_CpuLoad.Invoke() | Sort LoadPercentage -descending | Select -first 7|ConvertTo-Json -Compress

}

Function getCurTime {
$ctime=get-date -UFormat "%Y-%m-%d %H:%M:%S"
return $ctime
}

#Get the current cpu usage:
$curtime=getCurTime
$Output=(get-counter -Counter "\Processor(_Total)\% Processor Time" -SampleInterval 1)|Out-String
$Output=$Output.trim()
#echo $Output.Split()[-1]
$cpuusage=$Output.Split()[-1].trim()
$cpuusage=$cpuusage/1
$cpuusage=([math]::Round($cpuusage,[System.MidpointRounding]::AwayFromZero))


if($cpuusage -gt 0)
{

$topProcess=GetTop5Process

echo "Hiro can not resolve this issue, current cpu usage is: $cpuusage % at $curtime .end" 
echo "Below is the top 5 processes which are consuming more cpu resources .end"
echo $topProcess
exit
}
else
{
echo "no issue found, CPU Spike, Resolve the issue,current CPU usage is $cpuusage % at $curtime !! "
exit
}

