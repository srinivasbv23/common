import json
import urllib2
import ssl
import urllib
import requests
from urllib2 import urlopen, URLError, HTTPError
from socket import timeout


class UrlPowerOperationResp():
    s2 = requests.Session()

    def postPowerOperation(self, server, ema_amt_power_operation):
        powerOperation_headers = {'Content-Type': 'application/json'}
        try:
            respPowerOperation = UrlPowerOperationResp.s2.post(server, json=ema_amt_power_operation, headers=powerOperation_headers)
            respText = self.onSuccess(respPowerOperation)
            print(respPowerOperation.status_code)
            if(respPowerOperation.status_code==500):
                raise SystemError("Internal Server Error")
        except HTTPError as e:
            print("HTTP error block")
            respText = self.onError(respPowerOperation, e)
        except URLError as e:
            print("URL error block")
            respText = self.onError(respPowerOperation, e)
        except timeout as e:
            print("timeout error block")
            respText = "Request timed out!"
        except KeyError as e:
            print("key error block")
            respText = self.onError(respPowerOperation, e)
        except NameError as e:
            print("name error block")
            respText = self.onError(respPowerOperation, e)
        except SystemError as e:
            print("500 internal server error block")
            respText = self.onError(respPowerOperation, e)
        return respText

    def onError(self, resp, *args):
        reason = ""
        respText = ""

        if args != None:
            if type(args[0]) is URLError: reason = args[0].reason
        else:
            reason = "{0}; Code: {1}".format(args[0].reason, args[0].description)
        if (resp == None):
            respText =  "Error: {0}".format(reason)
        else:
            respText =  "Error: " + "Invalid Request"

        return respText

    def onSuccess(self, resp):
        return resp.content