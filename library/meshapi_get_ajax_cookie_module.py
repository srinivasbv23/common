

#!/usr/bin/python

from ansible.module_utils.basic import AnsibleModule
from zeep import Client
import zeep
import json
def main():


    module = AnsibleModule( argument_spec = dict(
            mesh_simple_api_url = dict(type='str'),
            mesh_user_name = dict(type='str'),
            mesh_passwd = dict( type='str')
        ) )
    
    client = Client(wsdl=module.params['mesh_simple_api_url'])
    response = client.service.GetAjaxCookie(module.params['mesh_user_name'],module.params['mesh_passwd'])
    a = zeep.helpers.serialize_object(response)
    json_object_a = json.loads(json.dumps(a))

    module.exit_json(changed=False, content=json_object_a)


if __name__ == '__main__':
    main()

