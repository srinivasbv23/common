import json
import urllib2
import ssl
import urllib
import requests
import ema_token_auth
from urllib2 import urlopen, URLError, HTTPError
from socket import timeout
import yaml


class UrlDeviceSearchResp(ema_token_auth.EmaTokenResp):

    def getDeviceSearch(self, server,token,search):


        str_token = 'Bearer ' + str(token['msg'])
        deviceSearch_headers = {'Authorization': str_token}
        try:
            respDeviceSearch = UrlDeviceSearchResp.s.get(server,headers=deviceSearch_headers)

            string_resp = str(respDeviceSearch)

            input_dict = json.loads(respDeviceSearch.text)
            if len(search)>=3:
             if len(input_dict) >=1:
              #print len(input_dict)
              output_dict = [x for x in input_dict if x['ComputerName'].startswith(search)]
              #print len(output_dict)
              if len(output_dict)>=1:
               output_json = json.dumps(output_dict)
               respText = self.onSuccess(output_json)
              else:respText="No device(s) found"
             else: respText="No device(s) found"
            else:respText="Error! Please enter minimum 3 characters to search"
        except HTTPError as e:
            print("HTTP error block")
            respText = self.onError(output_json, e)
        except URLError as e:
            print("URL error block")
            respText1 = self.onError(output_json, e)
        except timeout as e:
            print("timeout error block")
            respText = "Request timed out!"
        except KeyError as e:
            print("key error block")
            respText = self.onError(output_json, e)
        return respText

    def onError(self, resp, *args):
        reason = ""
        respText = ""

        if args != None:
            if type(args[0]) is URLError: reason = args[0].reason
            else: reason = "{0}; Code: {1}".format(args[0].reason, args[0].code)
        if (resp == None): respText =  "Error: {0}".format(reason)
        else: respText =  "Error: " + "Invalid parameter"

        return respText

    def onSuccess(self, resp):
        return resp

        respText.exit_json(respText)


