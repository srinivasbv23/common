

#!/usr/bin/python

from ansible.module_utils.basic import AnsibleModule
from zeep import Client, wsdl
import zeep
import json
import random
import string


def main():

    module = AnsibleModule(argument_spec=dict(
        mesh_simple_api_url=dict(type='str'),
        mesh_user_name=dict(type='str'),
        mesh_user_passwd=dict(type='str'),
        new_user_name=dict(type='str'),
        mesh_name=dict(type='str'),
        web_permissions1=dict(type='int'),
        web_permissions2=dict(type='int'),
        ))

    new_user_passwd = ''.join(random.choice(string.ascii_uppercase+ string.ascii_lowercase + string.digits) for _ in range(8))
    mesh_passwd = ''.join(random.choice(string.ascii_uppercase+string.ascii_lowercase+string.digits) for _ in range(8))
    client = Client(wsdl=module.params['mesh_simple_api_url'])

    createAccountResponse = client.service.CreateAccount(module.params['mesh_user_name'],module.params['mesh_user_passwd'],module.params['new_user_name'],new_user_passwd)
    createAccountResponseSrlz = zeep.helpers.serialize_object(createAccountResponse)
    json_object_create_account = json.loads(json.dumps(createAccountResponseSrlz))
    if json_object_create_account == 'Success':
        response = client.service.CreateMesh(module.params['new_user_name'],new_user_passwd,module.params['mesh_name'],mesh_passwd,module.params['web_permissions1'],module.params['web_permissions2'])
        a = zeep.helpers.serialize_object(response)
        json_object_a = json.loads(json.dumps(a))
        if json_object_a['CreateMeshResult'] == 'Success':
            json_object_a['NewUserPasswd'] = new_user_passwd
            module.exit_json(changed=True, content=json_object_a)
        else:
            module.fail_json(msg='Error while creating mesh.')
    else:
        module.fail_json(msg='Error while creating account.')


if __name__ == '__main__':
    main()


			
