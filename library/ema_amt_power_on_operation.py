import json
import urllib2
import ssl
import urllib
import requests
import ema_token_auth
from urllib2 import urlopen, URLError, HTTPError
from socket import timeout


class UrlPowerOnOperationResp(ema_token_auth.EmaTokenResp):

    def postPowerOnOperation(self, server, ema_amt_power_on_operation, token):

        str_token = 'Bearer ' + str(token['msg'])
        powerOnOperation_headers = {'Authorization': str_token}
        try:
            respPowerOnOperation = UrlPowerOnOperationResp.s.post(server, json=ema_amt_power_on_operation,
                                                                  headers=powerOnOperation_headers)
            respText = self.onSuccess(respPowerOnOperation)
            print respText
        except HTTPError as e:
            print("HTTP error block")
            respText = self.onError(respPowerOnOperation, e)
        except URLError as e:
            print("URL error block")
            respText = self.onError(respPowerOnOperation, e)
        except timeout as e:
            print("timeout error block")
            respText = "Request timed out!"
        except KeyError as e:
            print("key error block")
            respText = self.onError(respPowerOnOperation, e)
        except NameError as e:
            print("name error block")
            respText = self.onError(respPowerOnOperation, e)

        return respText

    def onError(self, resp, *args):
        reason = ""
        respText = ""

        if args != None:
            if type(args[0]) is URLError: reason = args[0].reason
        else:
            reason = "{0}; Code: {1}".format(args[0].reason, args[0].code)
        if (resp == None):
            respText = "Error: {0}".format(reason)
        else:
            respText = "Error: " + "Invalid Request"

        return respText

    def onSuccess(self, resp):
        return resp.content
