import json
import urllib2
import ssl
import urllib
import requests
from urllib2 import urlopen, URLError, HTTPError
from socket import timeout
from requests import Request, Session


class EmaTokenResp():
    s = requests.Session()
    access_token = 0


    def postReq(self, server, token_value):

        data = urllib.urlencode(token_value)

        try:

            r = EmaTokenResp.s.post(server, data)
            json_response = json.loads(r.text)
            EmaTokenResp.access_token = json_response['access_token']
            if(r.status_code==200):
             respText1 = self.onSuccess(json_response)
            elif(r.status_code==400):
                self.onError(json_response)

        except HTTPError as e:
            respText1 = self.onError(json_response, e)
        except URLError as e:
            respText1 = self.onError(json_response, e)
        except timeout as e:
            respText1 = "Request timed out!"
        except KeyError as e:
            print("key error block")
            respText1 = self.onError(json_response, e)
        except NameError as e:
            print("name error block")
            respText1 = self.onError(json_response, e)
        return respText1

    def onError(self, resp, *args):
        reason = ""
        respText1 = ""

        print(type(resp))

        if args != None:
            if type(args[0]) is URLError: reason = args[0].reason
        else:
            reason = "{0}; Code: {1}".format(args[0].reason, args[0].description)
        if (resp == None):
            respText1 = "Error: {0}".format(reason)
        else:
            respText1 = "Error: " + resp['error_description']

        return respText1

    def onSuccess(self, resp):
        return resp['access_token']

# obj1 = UrlResp()
# print obj1.postReq('https://wc-dev.compucom.com/api/token',payload)
