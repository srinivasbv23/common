#!/usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import absolute_import, division, print_function
__metaclass__ = type

ANSIBLE_METADATA = {'metadata_version': '1.1', 
                     'status': ['preview'],
                    'supported_by': 'community'}

import re

from ansible.module_utils.basic import AnsibleModule

def parse_fs(data,devname):
  for line in data.splitlines():
    if devname in line:
      sp=[]
      sp=line.split()
      fsname=sp[-1]
      return(0,fsname)
  return(1,"notFound")



def main():
  module = AnsibleModule(
    argument_spec=dict(
      devname=dict(type='str', required=True),),
      supports_check_mode=False
    )

  result = {
        'timestamp': "",
        'changed': False,
        'msg': ""
    }


  devname = module.params['devname']
  df_cmd = module.get_bin_path("df", required=True) 
  date_cmd = module.get_bin_path("date", required=True)
  rc=err=fs_info=''
  (rc, fs_info, err) = module.run_command("%s -k %s" % (df_cmd , devname))
  #rc1,fsName=parse(fs_info,devname)  
  rc1=err1=fs_info2=''
  format=" +'%F %H:%M:%S'"
  (rc1, fs_info2, err1) = module.run_command("%s %s" % (date_cmd, format))

  if rc == 0 and rc1 == 0:
    result['changed']=False
    result['msg']=fs_info
    result['timestamp']=fs_info2
    module.exit_json(**result)
  else:
    module.fail_json(msg="Failed to get the information. errmsg: %s, %s." %  (err, devname))
    
if __name__ == '__main__':
  main()
