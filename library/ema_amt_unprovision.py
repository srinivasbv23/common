import json
import urllib2
import ssl
import urllib
import requests
import ema_token_auth
from urllib2 import urlopen, URLError, HTTPError
from socket import timeout
import yaml


class Ema_Amt_Unprovision(ema_token_auth.EmaTokenResp):

    def postAMT_Unprovisioning(self, server, ema_unprovisioning, token):
        str_token = 'Bearer ' + str(token['msg'])
        print str_token
        amt_Unprovisioning_headers = {'Authorization': str_token}
        print amt_Unprovisioning_headers
        try:
            respUnprovisioning = Ema_Amt_Unprovision.s.post(server, json=ema_unprovisioning, headers=amt_Unprovisioning_headers)
            print respUnprovisioning.status_code
            respText = self.onSuccess(respUnprovisioning)

        except HTTPError as e:
            print("HTTP error block")
            respText = self.onError(respUnprovisioning, e)
        except URLError as e:
            print("URL error block")
            respText1 = self.onError(respUnprovisioning, e)
        except timeout as e:
            print("timeout error block")
            respText = "Request timed out!"
        except KeyError as e:
            print("key error block")
            respText = self.onError(respUnprovisioning, e)
        return respText

    def onError(self, resp, *args):
        reason = ""
        respText = ""

        if args != None:
            if type(args[0]) is URLError: reason = args[0].reason
            else: reason = "{0}; Code: {1}".format(args[0].reason, args[0].code)
        if (resp == None): respText =  "Error: {0}".format(reason)
        else: respText =  "Error: " + "Invalid parameter"

        return respText

    def onSuccess(self, resp):
        SuccessMsg="Unprovision Initated"
        if(resp.status_code==401):
            return resp.content
        elif (resp.status_code == 409):
            return resp.content
        elif(resp.status_code==204):
            return SuccessMsg



