import json
import urllib2
import ssl
import urllib
import requests
import ema_token_auth
from urllib2 import urlopen, URLError, HTTPError
from socket import timeout
import yaml


class UrlAmtProvisioningCertificatesResp(ema_token_auth.EmaTokenResp):

    def getAmtProvisioningCertificates(self, server, token):
        str_token = 'Bearer ' + str(token['msg'])
        amtCertificate_headers = {'Authorization': str_token}

        try:
            resAmtProvisioningCertificates = UrlAmtProvisioningCertificatesResp.s.get(server, headers=amtCertificate_headers)
            respText = self.onSuccess(resAmtProvisioningCertificates)
        except HTTPError as e:
            print("HTTP error block")
            respText = self.onError(resAmtProvisioningCertificates, e)
        except URLError as e:
            print("URL error block")
            respText1 = self.onError(resAmtProvisioningCertificates, e)
        except timeout as e:
            print("timeout error block")
            respText = "Request timed out!"
        except KeyError as e:
            print("key error block")
            respText = self.onError(resAmtProvisioningCertificates, e)
        return respText

    def onError(self, resp, *args):
        reason = ""
        respText = ""

        if args != None:
            if type(args[0]) is URLError: reason = args[0].reason
            else: reason = "{0}; Code: {1}".format(args[0].reason, args[0].code)
        if (resp == None): respText =  "Error: {0}".format(reason)
        else: respText =  "Error: " + "Invalid parameter"

        return respText

    def onSuccess(self, resp):

        return resp.content

        respText.exit_json(respText)



