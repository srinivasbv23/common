﻿#!powershell
# This file is part of Ansible
#
# (c) 2017, Dag Wieers <dag@wieers.com>
#
# Ansible is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Ansible is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Ansible.  If not, see <http://www.gnu.org/licenses/>.

# WANT_JSON
# POWERSHELL_COMMON

# This particular file snippet, and this file snippet only, is BSD licensed.
# Modules you write using this snippet, which is embedded dynamically by Ansible
# still belong to the author of the module, and may assign their own license
# to the complete work.
#
# Copyright (c), Michael DeHaan <michael.dehaan@gmail.com>, 2014, and others
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification,
# are permitted provided that the following conditions are met:
#
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright notice,
#      this list of conditions and the following disclaimer in the documentation
#      and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
# USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

[CmdletBinding()]
$params = Parse-Args $args

$systemInformation = New-Object psobject @{
    SystemInformation_Hardware_Single_02=Get-WmiObject Win32_ComputerSystem | select   Model,PCSystemType,SystemType,UserName 
    Processor_Hardware_Single_0=Get-WmiObject Win32_Processor | select   Architecture,ExtClock,L2CacheSize,Manufacturer,MaxClockSpeed,Name,ProcessorType,Status 
    HardDrive_Hardware_Single_3=Get-WmiObject Win32_DiskDrive | select   Name,Partitions,Size,Status 
    RAMPhysicalMemory_Hardware_Multiple_2=Get-WmiObject Win32_PhysicalMemory | select  Capacity,DeviceLocator,FormFactor,Manufacturer,MemoryType,PartNumber 
    PhysicalBus_Hardware_Multiple_2=Get-WmiObject Win32_Bus | select  BusType,DeviceID,PNPDeviceID,Status 
    DiskPartitions_Software_Multiple_0=Get-WmiObject Win32_LogicalDisk | select  DriveType,FileSystem,FreeSpace,Name,Size,VolumeSerialNumber 
    BusAdapter_Software_Multiple_0=Get-WmiObject CIM_Controller | select  Caption,PNPDeviceID 
    Printer_Software_Multiple_0=Get-WmiObject Win32_Printer | select  DriverName,PortName,PrinterStatus,Shared,ShareName 
    DisplayAdapter_Software_Multiple_0=Get-WmiObject Win32_VideoController | select  AdapterRAM,Caption,PNPDeviceID,Status,VideoProcessor 
    IntegratedDeviceElectronicsDriver_Software_Multiple_0=Get-WmiObject Win32_IDEController | select  Caption,PNPDeviceID 
    LoggedOnUsers_Software_Multiple_0=Get-WmiObject Win32_LoggedOnUser | select  Antecedent,Dependent 
    OperatingSystem_Software_Single_0=Get-WmiObject Win32_OperatingSystem | select  BuildNumber,CSDVersion,FreePhysicalMemory,FreeVirtualMemory,lastbootuptime,LocalDateTime,Name,OSArchitecture,OSType,ProductType,SerialNumber,ServicePackMajorVersion,ServicePackMinorVersion,TotalVirtualMemorySize,Version 
    NetworkAdapter_Software_Multiple_18=Get-WmiObject Win32_NetworkAdapterConfiguration | select  DefaultIPGateway,Description,DHCPServer,DNSHostName,IPAddress,IPEnabled,IPSubnet,MACAddress,ServiceName 
    PhysicalConnectionPorts_Hardware_Multiple_0=Get-WmiObject Win32_PortConnector | select  InternalReferenceDesignator,ConnectorType 
    Battery_Hardware_Single_0=Get-WmiObject Win32_Battery | select  Name,EstimatedRunTime,EstimatedChargeRemaining,Chemistry,Status 
    SoundAdapter_Software_Multiple_0=Get-WmiObject Win32_SoundDevice | select  Caption,PNPDeviceID,Manufacturer,Availability,Status 
    BaseBoard_Hardware_Single_0=Get-WmiObject Win32_BaseBoard | select  Manufacturer,SerialNumber,Status,Model 
    CDDVDDrives_Software_Single_0=Get-WmiObject Win32_CDROMDrive | select  Name,TransferRate 
    OpenNetworkConnections_Software_Multiple_0=Get-WmiObject Win32_NetworkConnection | select  AccessMask,ConnectionType,RemoteName,Name,Status 
    SystemDrivers_Hardware_Multiple_23=Get-WmiObject Win32_SystemDriver | select  Caption,PathName,ServiceType,Status 
	InstalledServices_Application_Multiple_0=Get-WmiObject Win32_Service | select DisplayName,PathName,StartMode,DesktopInteract,StartName
	InstalledPrograms_Application_Multiple_0=Get-WmiObject Win32_Product | select Name,InstallDate,InstallSource 
	InstalledHotfixes_Application_Multiple_0=Get-WmiObject Win32_QuickFixEngineering | select Description,HotFixID,InstalledOn,InstalledBy
    
}

Exit-Json $systemInformation