#!/usr/bin/bash
#this script is for restarting SNMP
#on Solaris
osver=`uname -a |awk '{print $3}'`
if [ ${osver} ==  "5.10" ]
then
/usr/sbin/svcadm restart sma
else
/usr/sbin/svcadm restart net-snmp
fi
