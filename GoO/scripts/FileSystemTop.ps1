<#



.SYNOPSIS



-Identify the top 5 biggest files from a local Drive

-Identify local Drive letters that are utilizing above a certain percentage of the file system

The purpose of this script is to be sent by Ansible as part of the HIRO automation processes.



.DESCRIPTION



1 - Identify Windows Drive letters that are above the FileSystem utilization threshold and

output the percentage details.

2 - Identify top 5 biggest files in a file system drive based on the DeviceID letter provided.



.PARAMETER getdeviceabovethreshold



Will identify the logical disk DeviceID with file system utilization above the provided threshold percentage

and return a list in a compressed Json format including only the drive letters that go beyond the threshold.



.PARAMETER gettop5fromdeviceid



Will return a list in a compressed Json format of the top 5 biggest files in the specified drive letter (DeviceID).



.EXAMPLE



.\FileSystemTop.ps1 -getdeviceabovethreshold 90

{"DeviceID":"D:","Utilization":"98.13 %"}



.EXAMPLE



.\FileSystemTop.ps1 -gettop5fromdeviceid "C:"

[{"Length":8589934592,"FullName":"C:\\pagefile.sys"},{"Length":575029155,"FullName":"C:\\Windows\\WinSxS\\ManifestCache\\378028f18a3c

c75c_blobs.bin"},{"Length":456314880,"FullName":"C:\\Windows\\LiveKernelReports\\WinsockAFD-20180718-2041.dmp"},{"Length":453619752,"

FullName":"C:\\Windows\\SoftwareDistribution\\Download\\1f53cdd60a7a459a19ee9ae37812be3898d50f85"},{"Length":245755904,"FullName":"C:

\\SQLServer2017Media\\ExpressAdv_ENU\\x64\\Setup\\SQL_POLYBASE_CORE_INST.MSI"}]



.NOTES



The process to get the top 5 files may take some minutes to complete and it's filtering files greater than

5MB by default.



###############################################################################

# Compucom

# FileName: FileSystemTopV1.2.ps1

# Version 1.2 / 10/10/2018 10:45:00

# Juan Abundis / juan.abundis@compucom.com

# Revision List:

# 10/08/2018 22:31:58 - Added Notes and modified the output format to Json.

# 10/09/2018 09:03:12 - Changed out-default for return to avoid issues with Ansible/Mule.

# 10/10/2018 10:45:28 - Updated the Help notes and modified File Name to identify versions.

# Tested on:

# OS                                           Version 

# --                                           ------- 

# Microsoft Windows Server 2008 R2 Datacenter  6.1.7601

# Microsoft Windows Server 2012 R2 Standard 6.3.9600

# Microsoft Windows 10 Pro 10.0.17134

###############################################################################



.LINK





#>

[CmdletBinding()]

Param(

  [Parameter(Mandatory=$False,ParameterSetName = "Set 1")][ValidateNotNullOrEmpty()]

  [string]$gettop5fromdeviceid,

  [Parameter(Mandatory=$False,ParameterSetName = "Set 2")][ValidateNotNullOrEmpty()]

  [Int]$getdeviceabovethreshold

)

function Get-DeviceAboveThreshold ($Threshold) {

    $DevicesThreshold = Get-WmiObject -Class win32_logicaldisk | Where-Object -FilterScript {($_.FreeSpace/$_.Size) -lt 1-($Threshold/100)} | Select-Object @{l='DeviceID';e={$_.DeviceID}},@{l='Utilization';e={"{0:P}" -f (($_.Size-$_.FreeSpace)/($_.Size))}} | ConvertTo-Json -Compress

    return $DevicesThreshold

}



function Get-Top5-BigFiles ($DeviceID){
    $DeviceID2=$DeviceID+'\'

    $getDiskUsage=(Get-WmiObject Win32_Volume |where {$_.name  -eq $DeviceID2} | Select-Object Name, @{Name="Size(MB)";Expression={"{0:N1}" -f($_.Capacity/1mb/200)}})

    echo $getDiskUsage |Out-File C:\Windows\Temp\diskSize.txt
    $result=Get-Content c:\Windows\Temp\diskSize.txt|select-string $DeviceID|Out-String

    $dskUsg=$result.trim().split()[-1]
    $dskUsg=[int]$dskUsg*1024*1024

    $Top5Files = Get-ChildItem -Path "$DeviceID\" -Recurse|where-Object { ($_.FullName -notmatch "C:\\Windows") -and ($_.FullName -notmatch "C:\\Program Files") }| Where-Object -FilterScript {$_.Length -gt $dskUsg} | Sort-Object -Descending -Property Length | Select-Object -Property Length,FullName -First 5 | ConvertTo-Json -Compress

    return $Top5Files

}

If($getdeviceabovethreshold){

    Get-DeviceAboveThreshold $getdeviceabovethreshold

}



If($gettop5fromdeviceid){

    Get-Top5-BigFiles $gettop5fromdeviceid

}
