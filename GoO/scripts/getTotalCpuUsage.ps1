Function getCurTime {
$ctime=get-date -UFormat "%Y-%m-%d %H:%M:%S"
return $ctime
}


#Get the current cpu usage:
$curtime=getCurTime
$Output=(get-counter -Counter "\Processor(_Total)\% Processor Time" -SampleInterval 1)|Out-String
$Output=$Output.trim()
#echo $Output.Split()[-1]
$cpuusage=$Output.Split()[-1].trim()
$cpuusage=$cpuusage/1
$cpuusage=([math]::Round($cpuusage,[System.MidpointRounding]::AwayFromZero))

echo "Current CPU usage is $cpuusage % at $curtime .end"
