#check current time frame
#
$drive=$args[0]
$drive1=$drive+'\'

Function getCurTime {
$a=""
$b=""
$ctime=""
$a=get-date
$b=get-date -UFormat "%H"
$ctime=get-date -UFormat "%Y-%m-%d %H:%M:%S"
return $ctime
}

Function getDiskUsage {
$getDskUsg=(Get-WmiObject Win32_Volume |where {$_.name  -eq $drive1} | Select-Object Name, @{Name="Size(GB)";Expression={"{0:N1}" -f($_.Capacity/1gb)}},@{Name="FreeSpace(GB)";Expression={"{0:N1}" -f($_.freespace/1gb)}},@{Name="UsedSpacePerCent";Expression={"{0:P0}" -f(1-$_.freespace/$_.capacity)}})
return $getDskUsg
}

Function getDiskUsage2 {
$getDskUsg=(Get-WmiObject Win32_Volume |where {$_.name  -eq $drive1} | Select-Object Name, @{Name="Size(GB)";Expression={"{0:N1}" -f($_.Capacity/1gb)}},@{Name="FreeSpace(GB)";Expression={"{0:N1}" -f($_.freespace/1gb)}},@{Name="UsedSpacePerCent";Expression={"{0:P0}" -f(1-$_.freespace/$_.capacity)}}|ConvertTo-Json -Compress)
return $getDskUsg
}

$curtime=""
$getDiskUsage=""
$getDiskUsage2=""
$curtime=getCurTime
$getDiskUsage=getDiskUsage
$getDiskUsage2=getDiskUsage2
echo $getDiskUsage |Out-File C:\Windows\Temp\diskSize.txt
echo $getDiskUsage2 |Out-File C:\Windows\Temp\diskSize2.txt
#sleep 3
#cat c:\Windows\Temp\diskSize.txt
$result=Get-Content c:\Windows\Temp\diskSize.txt|select-string ${drive}|Out-String
#echo $result
$dskUsg=$result.trim().split()[-2]
#echo $dskUsg
$dskUsg=$dskUsg/1
#echo $dskUsg

if($dskUsg -ge 95) {
echo "${drive} drive usage crosses threshold at $curtime .endend"
echo "Below is the details:end"
cat c:\Windows\Temp\diskSize2.txt
}
else {
echo "no issue found, waiting for INACTIVE event from SMARTs to resolve the issue. $curtime .endend"
echo "Below is the details:end"
cat c:\Windows\Temp\diskSize2.txt
}
