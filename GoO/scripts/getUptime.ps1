Get-CimInstance -ClassName win32_OperatingSystem | select csname, lastbootuptime|ConvertTo-Json  -Compress
$os = Get-WmiObject win32_operatingsystem
$uptime = (Get-Date) - ($os.ConvertToDateTime($os.lastbootuptime))
$Display = "endUptime: " + $Uptime.Days + " days, " + $Uptime.Hours + " hours, " + $Uptime.Minutes + " minutes" 
Write-Output $Display
