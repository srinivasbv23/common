
#Function

Function GetTop5ProcessB {
$Get_CpuLoad = {
   $M1 = Get-WmiObject -Query "select * from Win32_PerfRawData_PerfProc_Process"
   Start-Sleep -Seconds 1
   $M2 = Get-WmiObject -Query "select * from Win32_PerfRawData_PerfProc_Process" 
   
   for($i = 0;$i -lt $M1.Count;$i++){
       $Load = [math]::Round((($M2[$i].PercentProcessorTime - $M1[$i].PercentProcessorTime)/($M2[$i].Timestamp_Sys100NS - $M1[$i].Timestamp_Sys100NS)) *100/$env:NUMBER_OF_PROCESSORS,2)
       if($Load -lt 0){$Load = "Idle"}
       
       $CoreInfo = New-Object psobject -Property @{
           ProcessName    = ($M1[$i].name)
           PID            = ($M1[$i].IDProcess)
           ThreadCount    = ($M1[$i].ThreadCount)
           LoadPercentage = $Load
       }
       [array]$result += $CoreInfo
   }
   return $result 
}
$Get_CpuLoad.Invoke() | Sort LoadPercentage -descending |Select -first 7
#$Get_CpuLoad.Invoke() | Sort LoadPercentage -descending | Select -first 7|select-object -skip 1

}

Function GetTop5Process {
$Get_CpuLoad = {
   $M1 = Get-WmiObject -Query "select * from Win32_PerfRawData_PerfProc_Process"
   Start-Sleep -Seconds 1
   $M2 = Get-WmiObject -Query "select * from Win32_PerfRawData_PerfProc_Process" 
   
   for($i = 0;$i -lt $M1.Count;$i++){
       $Load = [math]::Round((($M2[$i].PercentProcessorTime - $M1[$i].PercentProcessorTime)/($M2[$i].Timestamp_Sys100NS - $M1[$i].Timestamp_Sys100NS)) *100/$env:NUMBER_OF_PROCESSORS,2)
       if($Load -lt 0){$Load = "Idle"}
       
       $CoreInfo = New-Object psobject -Property @{
           ProcessName    = ($M1[$i].name)
           PID            = ($M1[$i].IDProcess)
           ThreadCount    = ($M1[$i].ThreadCount)
           LoadPercentage = $Load
       }
       [array]$result += $CoreInfo
   }
   return $result 
}
#$Get_CpuLoad.Invoke() | Sort LoadPercentage -descending |Select -first 7|ConvertTo-Html -Fragment
$Get_CpuLoad.Invoke() | Sort LoadPercentage -descending | Select -first 7|ConvertTo-Json -Compress


}

Function GetTop3Process {
$Get_CpuLoad = {
   $M1 = Get-WmiObject -Query "select * from Win32_PerfRawData_PerfProc_Process"
   Start-Sleep -Seconds 1
   $M2 = Get-WmiObject -Query "select * from Win32_PerfRawData_PerfProc_Process" 
   
   for($i = 0;$i -lt $M1.Count;$i++){
       $Load = [math]::Round((($M2[$i].PercentProcessorTime - $M1[$i].PercentProcessorTime)/($M2[$i].Timestamp_Sys100NS - $M1[$i].Timestamp_Sys100NS)) *100/$env:NUMBER_OF_PROCESSORS,2)
       if($Load -lt 0){$Load = "Idle"}
       
       $CoreInfo = New-Object psobject -Property @{
           ProcessName    = ($M1[$i].name)
           PID            = ($M1[$i].IDProcess)
           ThreadCount    = ($M1[$i].ThreadCount)
           LoadPercentage = $Load
       }
       [array]$result += $CoreInfo
   }
   return $result 
}
$Get_CpuLoad.Invoke() | Sort LoadPercentage -descending |Select -first 5|ConvertTo-Json -Compress 
#$Get_CpuLoad.Invoke() | Sort LoadPercentage -descending | Select -first 5| ConvertTo-Html -Fragment

}

Function getCurTime {
$ctime=get-date -UFormat "%Y-%m-%d %H:%M:%S"
return $ctime
}

#echo "start check CPU usage."

#Get the current cpu usage:
$curtime=getCurTime
$Output=(get-counter -Counter "\Processor(_Total)\% Processor Time" -SampleInterval 1)|Out-String
$Output=$Output.trim()
#echo $Output.Split()[-1]
$cpuusage=$Output.Split()[-1].trim()
$cpuusage=$cpuusage/1
$cpuusage=([math]::Round($cpuusage,[System.MidpointRounding]::AwayFromZero))

if($cpuusage -ge 90)
{

$topProcess=GetTop5ProcessB
echo "Current CPU usage is $cpuusage % at $curtime .end"

echo $topProcess |Out-File c:\Windows\Temp\toprocess.out
#echo $topProcess
$appProcess=@("Netiqccm","netiqmc","besclient","watchdog","chrome")
ForEach( $proc  in $appProcess)
{

$result=Get-Content c:\Windows\Temp\toprocess.out|select-string $proc
if ($result -ne $null){
$result100=(Get-Content c:\Windows\Temp\toprocess.out |Select-String $proc)|Out-String
#echo $result100
$proUsage=$result100.trim().split()[0]
#$test=$result100.trim().split()[-1]
#echo "$proUsage $test"
$proUsage=$proUsage/1
$procUsage=([math]::Round($procUsage,[System.MidpointRounding]::AwayFromZero))

#echo "$proUsage 100"
if($proUsage -gt 10)
{
$curtime=getCurTime
echo "Restarting $proc because it is consuming $proUsage % at $curtime !!end"
restart-service -Name $proc
}
}

}

#check again

$Output=(get-counter -Counter "\Processor(_Total)\% Processor Time" -SampleInterval 1)|Out-String
$curtime=getCurTime
$Output=$Output.trim()
#echo $Output.Split()[-1]
$cpuusage=$Output.Split()[-1].trim()
$cpuusage=$cpuusage/1
$cpuusage=([math]::Round($cpuusage,[System.MidpointRounding]::AwayFromZero))

if($cpuusage -ge 90)
{

#check current time frame
$a=get-date
$b=get-date -UFormat "%H"
#echo "it's $a , time   $b"
echo "current CPU usage is $cpuusage %!! at $curtime . end"

$curtime=get-date -UFormat "%Y-%m-%d %H:%M:%S"

if(($a.DayOfWeek -eq 'Friday' -and [int]$b -ge 18) -or ($a.DayOfWeek -eq 'Saturday' -and [int]$b -lt 9))
{
echo "It is the time to scan virus, $curtime. end"

clear-content c:\Windows\Temp\toprocess.out
$topProcess3=GetTop3Process
$curtime=getCurTime
echo $topProcess3|Out-File c:\Windows\Temp\toprocess.out
$resulta=Get-Content c:\Windows\Temp\toprocess.out |Select-String mcshield 
$reusltb=Get-Content c:\Windows\Temp\toprocess.out|Select-String scan32 
$resultc=Get-Content c:\Windows\Temp\toprocess.out|Select-String scan64

#cat c:\tmp\toprocess.out
if ($resulta -ne $null) {
#if (($resulta -ne $null) -and ($resultb -ne $null -or $resultc -ne $null)){
echo "McAfee process is in the top 3 processes causing high CPU usage at $curtime . end "
cat c:\Windows\Temp\toprocess.out
exit
}

}

#end McAfee check

clear-content c:\Windows\Temp\toprocess.out
$topProcess=GetTop5Process
$curtime=getCurTime


echo "CPU usage is $cpuusage % at $curtime , Hiro can not resolve this issue!end"
echo "Below is the top 5 processes which consume most of the CPU resource. "
echo $topProcess

exit

}else
{
echo "CPU Spike, current CPU usage is $cpuusage % at $curtime , waiting for INACTIVE events from SMARTs to resolve the issue!!"
exit
}

#End second check

}
else
{
echo "CPU Spike, current CPU usage is $cpuusage % at $curtime , waiting for INACTIVE events from SMARTs to resolve the issue!!"
exit
}
