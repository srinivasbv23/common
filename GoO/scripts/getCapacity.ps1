Get-WmiObject Win32_Volume |where {$_.name  -eq 'C:\'} | Select-Object @{Name="FreeSpacePerCent";Expression={"{0:P0}" -f($_.freespace/$_.capacity)}}|ft -HideTableHeaders
