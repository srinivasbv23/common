#!/usr/bin/python
# -*- coding: utf-8 -*-

#

from __future__ import absolute_import, division, print_function
__metaclass__ = type

ANSIBLE_METADATA = {'metadata_version': '1.1',
                    'status': ['stableinterface'],
                    'supported_by': 'core'}

import glob
import os
import platform
import re
import select
import shlex
import string
import subprocess
import tempfile
import time
import math


from ansible.module_utils.basic import AnsibleModule, load_platform_subclass
from ansible.module_utils.service import fail_if_missing
from ansible.module_utils.six import PY2, b
from ansible.module_utils._text import to_bytes, to_text


class FSUsage(object):
  platform = 'Generic'
  distribution = None
  def __new__(cls, *args, **kwargs):
    return load_platform_subclass(FSUsage, args, kwargs)

  def __init__(self, module):
    self.module = module
    self.action = module.params['action']
    self.devname = module.params['devname']
        

  def getVGName(self):
    self.module.fail_json(msg="getVGName not implemented on target platform")

  def getTop5BigFiles(self):
    self.module.fail_json(msg="getTop5BigFiles not implemented on target platform")
 
  def delApprovedTemFiles(self):
    self.module.fail_json(msg="delApprovedTemFiles not implemented on target platform")

  def getCurrentFSUsage(self):
    self.module.fail_json(msg="getCurrentFSUsage not implemented on target platform")

  


class AIXFSUsage(FSUsage):
  platform = 'AIX'
  distribution = None


  def getVGName(self):
    lslv_cmd = self.module.get_bin_path("lslv" , required=True)
    devname = self.devname
    devname = re.sub('_+','_',devname)
    sp = []
    sp = str(devname).split('/')
    extraFlags = sp[2]
    rc=err=lslv_output=""
    (rc, lslv_output, err) = self.module.run_command("%s  %s" %(lslv_cmd, extraFlags))
    
    filepath="/tmp/"+devname+"lslv.out"    
    file=open(filepath, "wb+")
    file.write(lslv_output)
    file.close()
    lines = [] 
    with open(filepath) as f:
      for line in f:
        line=re.sub(' +', ' ', line)
        lines.append(line.strip())
    result="nodata"
    for line in lines:
      if "VOLUME GROUP" in line:
        sp=[]
        sp=line.split(' ')
        result=str(sp[-1])
        break
    b={}
    b={"vgname":result}
    return(b)


  def getTop5BigFiles(self):
    df_cmd = self.module.get_bin_path("df", required=True)
    devname=self.devname
    devname=re.sub('_+','_',devname)
    rc=err=df_output=''
    (rc, df_output, err) = self.module.run_command("%s -m %s" % (df_cmd, devname ))
    
    filepath="/tmp/"+devname+"df.out"
    file=open(filepath,"wb+")
    file.write(df_output)
    file.close()
    lines = list()
    fssize=''
    fsname=''
    with open(filepath) as f:
      for line in f:
        if devname in line:
          line=re.sub(' +', ' ', line)
          sp=[]
          sp=line.split(' ')
          fssize=sp[1]
          fssize=math.ceil(float(fssize)/1024)
          fssize=fssize*1024*1024/512
          fsname=sp[-1]
    
    find_cmd = self.module.get_bin_path("find" , required=True)
    sudo_cmd = self.module.get_bin_path("sudo" , required=True)
    rc=err=find_output=''
    rc, find_output, err = self.module.run_command("%s %s %s -xdev -type f -size +%s" % (sudo_cmd, find_cmd, fsname, fssize))

    filepath2="/tmp/"+devname+"find.out"
    file=open(filepath2,"wb+")
    file.write(find_output)
    file.flush()
    file.close
    lines=[]
    time.sleep(2)
    ls_cmd = self.module.get_bin_path("ls" , required=True)
    with open(filepath2, "rb+") as inputFile:
      for line in inputFile:
        fileName=''
        fileName=line.strip()
        rc=err=ls_output=''
        rc, ls_output, err = self.module.run_command("%s %s -lrt \'%s\'" % (sudo_cmd, ls_cmd,fileName))
        ls_output=re.sub(' +', ' ', ls_output) 
        lines.append(ls_output.strip())
    num=0
    result=[]
    for line in sorted(lines, key=lambda line: float(line.split(' ')[4]),reverse=True):
      if num<5:
        #print(line)
        sp=[]
        sp=line.split(' ')
        fSize=float(sp[4])/1024/1024
        fName=sp[-1]
        b={}
        b={"fileSize":str(fSize),"fileName":str(fName)}
        result.append(dict(b))
      else:
        break
      num += 1
    return(result)

  def DelApprovedTmpFiles(self):
    pass

  def getCurrentFSUsage(self):
    df_cmd = self.module.get_bin_path("df", required=True)
    devname=self.devname
    devname=re.sub('_+','_',devname)
    rc=err=df_output=''
    (rc, df_output, err) = self.module.run_command("%s -g %s" % (df_cmd, devname ))
    
    filepath="/tmp/"+devname+"df.out"
    file=open(filepath,"wb+")
    file.write(df_output)
    file.flush()
    file.close() 
    time.sleep(2)
    result=[]
    with open(filepath,"rb+") as inputFile:
      for line in inputFile:
        line=re.sub(' +', ' ', line)
        if "Filesystem" not in line:
          b={}
          sp=[]
          sp=line.split(' ')
          fsname=sp[0]
          size=sp[1]
          avail=sp[2]
          used=float(sp[1])-float(sp[2])
          usedpercentage=sp[3]
          mountedon=sp[6].rstrip('\n')
          b={"Filesystem":fsname,"size":size,"used":used,"Free":avail,"used%":usedpercentage,"Mounted on":mountedon}
          #result.append(dict(b))
    return(b)

class LinuxFSUsage(FSUsage):
  platform = 'Linux'
  distribution = None
  
  def getCurrentFSUsage(self):
    pass

  def getVGName(self):
    pass

  def getTop5BigFiles(self):
    pass
  
  def DelApprovedTmpFiles(self):
    pass

class SolarisFSUsage(FSUsage):
  platform = 'SunOS'
  distribution = None
  
  def getVGName(self):
    b={}
    b={"vgname":"nodata"}
    return(b)

  def getTop5BigFiles(self):
    pass
  
  def DelApprovedTmpFiles(self):
    pass

  def getCurrentFSUsage(self):
    df_cmd = self.module.get_bin_path("df", required=True)
    devname=self.devname
    devname=re.sub('_+','_',devname)
    rc=err=df_output=''
    (rc, df_output, err) = self.module.run_command("%s -h %s" % (df_cmd, devname ))


    filepath="/tmp/"+devname+"df.out"
    file=open(filepath,"wb+")
    file.write(df_output)
    file.flush()
    file.close() 
    time.sleep(2)
    result=[]
    with open(filepath,"rb+") as inputFile:
      for line in inputFile:
        line=re.sub(' +', ' ', line)
        if "Filesystem" not in line:
          b={}
          sp=[]
          sp=line.split(' ')
          fsname=sp[0]
          size=sp[1]
          used=sp[2]
          avail=sp[3]
          usedpercentage=sp[4]
          mountedon=sp[5].rstrip('\n')
          b={"Filesystem":fsname,"size":size,"used":used,"Free":avail,"used%":usedpercentage,"Mounted on":mountedon}
          #result.append(dict(b))
    return(b)
          
          

def main():
  module = AnsibleModule(
    argument_spec=dict(
      action=dict(type='str', required=True , choices=['GetTop5BigFiles','GetVGName','DelApprovedTmpFiles','GetCurrentFSUsage']),
      devname=dict(type='str', required=True), 
      ),
    supports_check_mode=False,
    )

  result = {
    'changed': False,
    'msg': "",
    'timestamp': ""
    }

  operName = module.params['action']

  fsusage = FSUsage(module)

  module.debug('fsusage instantiated - platform %s' % fsusage.platform)
  if fsusage.distribution:
    module.debug('fsusage instantiated - distribution %s' % fsusage.distribution)

  date_cmd=module.get_bin_path("date", required=True)
  extraFlag=" +'%F %H:%M:%S'"

  rc=timestamp=err=''
  (rc, timestamp, err) = module.run_command("%s  %s" % (date_cmd, extraFlag))
  result['timestamp']=timestamp
  #if err:
  #  result['msg']=err

  if "GetVGName" in operName:
    resultVGName=fsusage.getVGName()
    result['changed']=False
    result['msg']=resultVGName
  elif "GetTop5BigFiles" in operName:
    resultTop5BigFiles=fsusage.getTop5BigFiles()
    result['changed']=False
    result['msg']=resultTop5BigFiles
  elif 'DelApprovedTmpFiles' in operName:
    result['changed']=True
    pass
  elif 'GetCurrentFSUsage' in operName:
    result['changed']=False
    resultCurrentFSUsage=fsusage.getCurrentFSUsage()
    vgname=fsusage.getVGName()
    #resultCurrentFSUsage.append(dict(vgname))
    resultCurrentFSUsage.update(vgname)
    result['msg']=resultCurrentFSUsage
    


  module.exit_json(**result)


if __name__ == '__main__':
  main()
