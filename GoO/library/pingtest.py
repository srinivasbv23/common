#!/usr/bin/python
# -*- coding: utf-8 -*-

#

from __future__ import absolute_import, division, print_function
__metaclass__ = type

ANSIBLE_METADATA = {'metadata_version': '1.1',
                    'status': ['stableinterface'],
                    'supported_by': 'core'}

import glob
import os
import platform
import re
import select
import shlex
import string
import subprocess
import tempfile
import time


from ansible.module_utils.basic import AnsibleModule, load_platform_subclass
from ansible.module_utils.service import fail_if_missing
from ansible.module_utils.six import PY2, b
from ansible.module_utils._text import to_bytes, to_text

def main():
  module = AnsibleModule(
    argument_spec=dict(
      action=dict(type='str', required=True , choices=['PingTest', 'sudoTest'])),
      supports_check_mode=False,
    )

  result = {
    'name': module.params['action'],
    'changed': False,
    'msg': "",
    }

  operName = module.params['action']
  if "PingTest" in operName: 
    sudo_cmd=module.get_bin_path("sudo", required=True)
    touch_cmd=module.get_bin_path("touch",required=True)
    rm_cmd=module.get_bin_path("rm",required=True)
    extra_flag1="/pingtest.out"
    extra_flag2="-f /pingtest.out"
    rc=err=0
    rc, output, err = module.run_command("%s %s %s" % (sudo_cmd, touch_cmd, extra_flag1))
    rc=err=0
    rc, output, err = module.run_command("%s %s %s" % (sudo_cmd, rm_cmd, extra_flag2))
    result['msg']=output
  
  module.exit_json(**result)


if __name__ == '__main__':
  main()
