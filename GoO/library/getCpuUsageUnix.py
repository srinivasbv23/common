#!/usr/bin/python
# -*- coding: utf-8 -*-

#

from __future__ import absolute_import, division, print_function
__metaclass__ = type

ANSIBLE_METADATA = {'metadata_version': '1.1',
                    'status': ['stableinterface'],
                    'supported_by': 'core'}

import glob
import os
import platform
import re
import select
import shlex
import string
import subprocess
import tempfile
import time


from ansible.module_utils.basic import AnsibleModule, load_platform_subclass
from ansible.module_utils.service import fail_if_missing
from ansible.module_utils.six import PY2, b
from ansible.module_utils._text import to_bytes, to_text


class CpuUsage(object):
  platform = 'Generic'
  distribution = None
  def __new__(cls, *args, **kwargs):
    return load_platform_subclass(CpuUsage, args, kwargs)

  def __init__(self, module):
    self.module = module
    self.action = module.params['action']
        

  def getCpuUsage(self):
    self.module.fail_json(msg="getCpuUsage not implemented on target platform")

  def getTop5Proc(self):
    self.module.fail_json(msg="getTop5Proc not implemented on target platform")

  


class AIXCpuUsage(CpuUsage):
  platform = 'AIX'
  distribution = None


  def getCpuUsage(self):
    sar_cmd = self.module.get_bin_path("sar" , required=True)
    sudo_cmd = self.module.get_bin_path("sudo" , required=True)
    extraFlags="ALL 4 2"
    rc=err=cpuUsage_output=""
    rc, cpuUsage_output, err = self.module.run_command("%s %s -P %s" %(sudo_cmd, sar_cmd, extraFlags))
    
    file=open("/tmp/ansibletmp.out", "wb+")
    file.write(cpuUsage_output)
    file.close()
    lines = list()
    with open("/tmp/ansibletmp.out") as f:
      for line in f:
        line=re.sub(' +', ' ', line)
        #print(line)
        lines.append(line.strip())
    flag1=False
    result="nodata"
    for line in lines:
      if "Average" in line:
        flag1=True
      if flag1==True and '-' in line and 'U' not in line:
        sp=[]
        sp=line.split(' ')
        usr=sp[1]
        sys=sp[2]
        entCpu=sp[6]
        #cpuUsage=(float(usr)+float(sys))*float(entCpu)/100
        if float(entCpu) > 100:
          cpuUsage=float(usr)+float(sys)
        else:
          cpuUsage=(float(usr)+float(sys))*float(entCpu)/100
        #print(str(cpuUsage))
        result=str(cpuUsage)
        break
    return(result)


  def getTop5Proc(self):
    ps_cmd = self.module.get_bin_path("ps", required=True)
    outputFormat1="user,pcpu,pid,etime,args" 
    rc=err=cpuTop5Proc_output=''
    rc, cpuTop5Proc_output, err = self.module.run_command("%s -Af -o %s" % (ps_cmd,outputFormat1 ))

    file=open("/tmp/ansible.out","wb+")
    file.write(cpuTop5Proc_output)
    file.close()
    lines = list()
    with open("/tmp/ansible.out") as f:
      for line in f:
        if "defunc" not in line and "exiting" not in line:
          line=re.sub(' +', ' ', line)
          #print(line)
          lines.append(line.strip())
    num=0
    result=[]
    for line in sorted(lines, key=lambda line: line.split()[1],reverse=True):
      if num<5:
        #print(line)
        sp=[]
        sp=line.split(' ')
        user=sp[0]
        pcpu=sp[1]
        pid=sp[2]
        etime=sp[3]
        cmd=sp[4]
        if "wait" in cmd:
          continue
        try:
          if sp[5]:
            cmd=sp[4]+' '+sp[5]
        except:
          pass
        b={}
        b={"user":user,"pcpu":pcpu,"pid":pid,"etime":etime,"cmd":cmd}
        result.append(dict(b))
        #result += user+' , '+pcpu+' , '+pid+' , '+time+' , '+cmd+'\n'
      else:
        break
      num += 1
    return(result)


class LinuxCpuUsage(CpuUsage):
  platform = 'Linux'
  distribution = None


  def getCpuUsage(self):
    sar_cmd = self.module.get_bin_path("sar" , required=True)
    sudo_cmd = self.module.get_bin_path("sudo" , required=True)
    extraFlags="ALL 4 2"
    rc=err=cpuUsage_output=""
    rc, cpuUsage_output, err = self.module.run_command("%s %s -P %s" %(sudo_cmd, sar_cmd, extraFlags))
    
    file=open("/tmp/ansibletmp.out", "wb+")
    file.write(cpuUsage_output)
    file.close()
    lines = list()
    with open("/tmp/ansibletmp.out") as f:
      for line in f:
        line=re.sub(' +', ' ', line)
        #print(line)
        lines.append(line.strip())
    flag1=False
    result="nodata"
    for line in lines:
      if "Average" in line:
        flag1=True
      if flag1==True and 'all' in line:
        sp=[]
        sp=line.split(' ')
        usr=sp[2]
        sys=sp[4]
        cpuUsage=float(usr)+float(sys)
        #print(str(cpuUsage))
        result=str(cpuUsage)
        break
    return(result)


  def getTop5Proc(self):
    ps_cmd = self.module.get_bin_path("ps", required=True)
    outputFormat1="user,pcpu,pid,etime,cmd" 
    rc=err=cpuTop5Proc_output=''
    rc, cpuTop5Proc_output, err = self.module.run_command("%s -Ao %s" % (ps_cmd,outputFormat1 ))

    file=open("/tmp/ansible.out","wb+")
    file.write(cpuTop5Proc_output)
    file.close()
    lines = list()
    with open("/tmp/ansible.out") as f:
      for line in f:
        line=re.sub(' +', ' ', line)
        #print(line)
        lines.append(line.strip())
    num=0
    result=[]
    for line in sorted(lines, key=lambda line: line.split()[1],reverse=True):
      if num<5:
        #print(line)
        sp=[]
        sp=line.split(' ')
        user=sp[0]
        pcpu=sp[1]
        pid=sp[2]
        etime=sp[3]
        cmd=sp[4]
        try:
          if sp[5]:
            cmd=sp[4]+' '+sp[5]
        except:
          pass
        b={}
        b={"user":user,"pcpu":pcpu,"pid":pid,"etime":etime,"cmd":cmd}
        result.append(dict(b))
        #result += user+' , '+pcpu+' , '+pid+' , '+time+' , '+cmd+'\n'
      else:
        break
      num += 1
    return(result)


def main():
  module = AnsibleModule(
    argument_spec=dict(
      action=dict(type='str', required=True , choices=['GetTotalCPUUsage', 'GetTop5CpuConsumer'])),
      supports_check_mode=False,
    )

  result = {
    'name': module.params['action'],
    'changed': False,
    'msg': "",
    'timestamp': ""
    }

  operName = module.params['action']

  cpuusage = CpuUsage(module)

  module.debug('CpuUsage instantiated - platform %s' % cpuusage.platform)
  if CpuUsage.distribution:
    module.debug('CpuUsage instantiated - distribution %s' % cpuusage.distribution)

  date_cmd=module.get_bin_path("date", required=True)
  extraFlag=" +'%F %H:%M:%S'"
  rc=timestamp=err=''
  (rc, timestamp, err) = module.run_command("%s  %s" % (date_cmd, extraFlag))
  result['timestamp']=timestamp

  if "GetTotalCPUUsage" in operName:
    resultCpuUsage=cpuusage.getCpuUsage()
    result['changed']=False
    result['msg']=resultCpuUsage
  elif "GetTop5CpuConsumer" in operName:
    resultTop5Proc=cpuusage.getTop5Proc()
    result['changed']=False
    result['msg']=resultTop5Proc

  module.exit_json(**result)


if __name__ == '__main__':
    main()
