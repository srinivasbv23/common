#!/usr/bin/python

from ansible.module_utils.basic import AnsibleModule
import os
def main():
    module = AnsibleModule( argument_spec = dict(
         drive = dict(type='str')
      ) )
defragmentation=os.popen('defrag.exe /C /A').read()
module.exit_json(changed=True, content=defragmentation)

if __name__ == '__main__':
    main()

			