#!/usr/bin/python
# -*- coding: utf-8 -*-

#

from __future__ import absolute_import, division, print_function
__metaclass__ = type

ANSIBLE_METADATA = {'metadata_version': '1.1',
                    'status': ['stableinterface'],
                    'supported_by': 'core'}

import glob
import os
import platform
import re
import select
import shlex
import string
import subprocess
import tempfile
import time


from ansible.module_utils.basic import AnsibleModule, load_platform_subclass
from ansible.module_utils.service import fail_if_missing
from ansible.module_utils.six import PY2, b
from ansible.module_utils._text import to_bytes, to_text

def getCoreDumpInfo(module):
  errpt_cmd = module.get_bin_path("errpt" , required=True)
  errorID = module.params['errorID']
  rc=err=coreDumpInfo_output=""
  rc, coreDumpInfo_output, err = module.run_command("%s -a -j %s" %(errpt_cmd, errorID))
    
  file=open("/tmp/errInfo.out", "wb+")
  file.write(coreDumpInfo_output)
  file.flush()
  file.close()
  time.sleep(2)
  num=0
  coreFileFlag=False
  progFileFlag=False
  coreFile="nodata"
  progName="nodata"
  with open("/tmp/errInfo.out", "rb+") as inputFile:
    for line in inputFile:
      if "LABEL:" in line and num==1:
        break
      elif "LABEL:" in line and num==0:
        num=1
      if "CORE FILE NAME" in line:
        coreFileFlag=True
        continue
      elif "PROGRAM NAME" in line:
        progFileFlag=True
        continue
      if coreFileFlag==True:
        coreFile=line.strip()
        coreFileFlag=False
      if progFileFlag==True:
        progName=line.strip()
        progFileFlag=False
      if coreFile != "nodata" and progName != "nodata":
        break
  result={}
  result={"coreFile":coreFile,"progName":progName}
  return(result)


def getPagingSpaceUsage(module):
  lsps_cmd = module.get_bin_path("lsps", required=True)
  rc=err=cpuTop5Proc_output=''
  rc, pagingSpaceUsage_output, err = module.run_command("%s -s" % lsps_cmd)

  file=open("/tmp/lsps.out","wb+")
  file.write(pagingSpaceUsage_output)
  file.flush()
  file.close()
  time.sleep(2)
  
  line2=""
  totalPSpace=""
  percentageOfPSpace=""
  aboveThreshold=False
  with open("/tmp/lsps.out","rb+") as inputFile:
    for line in inputFile:
      if "Paging Space" in line:
        continue
      elif "MB" in line:
        line=line.strip()
        line2=re.sub(' +',' ',line)
        line3=[]
        line3=line2.split(' ')
        totalPSpace=line3[0]
        percentageOfPSpace=line3[1].strip('%')
        if int(percentageOfPSpace) >= 70:
          aboveThreshold=True
        break
  result={}
  result={"totalPSpace":totalPSpace,"percentageOfPSpace":percentageOfPSpace,"aboveThreshold":aboveThreshold}
  return(result)  

def main():
  module = AnsibleModule(
    argument_spec=dict(
      errorID=dict(type='str', required=True , choices=['A924A5FC', 'C5C09FFA'])),
      supports_check_mode=False,
    )

  result = {
    'name': module.params['errorID'],
    'changed': False,
    'msg': "",
    'timestamp': ""
    }

  errorID = module.params['errorID']

  date_cmd=module.get_bin_path("date", required=True)
  extraFlag=" +'%F %H:%M:%S'"
  rc=timestamp=err=''
  (rc, timestamp, err) = module.run_command("%s  %s" % (date_cmd, extraFlag))
  result['timestamp']=timestamp

  if "A924A5FC" in errorID:
    resultCoreDumpInfo=getCoreDumpInfo(module)
    result['changed']=False
    result['msg']=resultCoreDumpInfo
  elif "C5C09FFA" in errorID:
    resultPagingSpaceUsage=getPagingSpaceUsage(module)
    result['changed']=False
    result['msg']=resultPagingSpaceUsage

  module.exit_json(**result)


if __name__ == '__main__':
  main()
