#!/usr/bin/python
# -*- coding: utf-8 -*-

from ansible.module_utils.basic import AnsibleModule
import sys
import subprocess


def main():

    module = AnsibleModule(argument_spec=dict(drive=dict(type='str')))

    if sys.maxsize > 2 ** 32:
        run_cmd = 'defrag /C'
    else:
        run_cmd = r'%systemroot%\sysnative\defrag /C'
    (output, err) = subprocess.Popen(run_cmd, std_out=subprocess.PIPE,
            shell=True).communicate()
    if err:
        module.fail_json(msg=format(err))
    else:
        module.exit_json(changed=True, content=output)


if __name__ == '__main__':
    main()
