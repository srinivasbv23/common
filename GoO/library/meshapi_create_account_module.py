#!/usr/bin/python

from ansible.module_utils.basic import AnsibleModule
from zeep import Client, wsdl
import zeep
import json
def main():


    module = AnsibleModule( argument_spec = dict(
         mesh_simple_api_url = dict(type='str'),
         mesh_user_name = dict(type='str'),
         mesh_passwd = dict( type='str'),
         new_user_name = dict( type='str'),
         new_passwd = dict( type='str')
      ) )
    
    client = Client(wsdl=module.params['mesh_simple_api_url'])
    response = client.service.CreateAccount(module.params['mesh_user_name'],module.params['mesh_passwd'],module.params['new_user_name'],module.params['new_passwd'])
    a = zeep.helpers.serialize_object(response)
    json_object_a = json.loads(json.dumps(a))
    if(json_object_a != "UnexpectedError"):
     module.exit_json(changed=True, content=json_object_a)
    else:
     module.fail_json(msg='Error while creating account.')

if __name__ == '__main__':
    main()


