#!/bin/bash
# ---------------------------------------------------------------------- #
# setup helper Connector node
# ---------------------------------------------------------------------- #
# NOTE: not suitable for a HA deployment, yet.
# for secondary nodes we have the following differences
# - do not set user password in WSO2IS (instead copy setting from primary node)
# - do not generate SSL cert (instead copy setting from primary node)
#
# other potential improvements
# - add activate/deactivate option for single components
#   e.g. currently you can select 'incident-consumer+backsync' but for
#   'config' only consumer
# - if user reruns the configuration script with different setting
#   unneeded services are not deactivated

umask 022
SETUP_TOOL_RPM="autopilot-config"
IAM_TOOL_RPM="autopilot-wso2is-tools"
CONNECT_CFG_RESPONSE_FILE="/opt/autopilot/conf/connectit.setup.rsp"

SETUP_TOOL_MISSING=""
PREPARE_SCRIPT="/opt/autopilot/setup/prepare-setup.sh"
RETRIEVE_OAUTH_HELPER="/opt/autopilot/setup/helpers/retrieve_oauth"
INCLUDE_COMMON="/opt/autopilot/setup/bash_include/_setup_common.sh"
CERT_CREATION_HELPER="/opt/autopilot/setup/helpers/create_selfsigned_ssl_certificate.sh"
CERT_EXPORT_HELPER="/opt/autopilot/setup/helpers/export_ssl_certificate.sh"
KAFKA_INCLUDE_SCRIPT="/opt/autopilot/setup/bash_include/kafka.sh"
KAFKA_IDGEN_HELPER="/opt/autopilot/setup/bash_include/gen_int_id.sh"
export KAFKA_SERVER_PROPERTIES="/opt/autopilot/kafka/config/server.properties"

INIT_SCRIPT_BUILDER="/opt/autopilot/connectit/setup/helpers/cit_init_script_builder.sh"

if [ ! -r $INCLUDE_COMMON ]; then
    SETUP_TOOL_MISSING="$SETUP_TOOL_MISSING $INCLUDE_COMMON="
fi
if [ ! -x $PREPARE_SCRIPT ]; then
    SETUP_TOOL_MISSING="$SETUP_TOOL_MISSING $PREPARE_SCRIPT"
fi
if [ ! -x $RETRIEVE_OAUTH_HELPER ]; then
    SETUP_TOOL_MISSING="$SETUP_TOOL_MISSING $RETRIEVE_OAUTH_HELPER"
fi
if [ ! -x $CERT_CREATION_HELPER ]; then
    SETUP_TOOL_MISSING="$SETUP_TOOL_MISSING $CERT_CREATION_HELPER"
fi
if [ ! -x $CERT_EXPORT_HELPER ]; then
    SETUP_TOOL_MISSING="$SETUP_TOOL_MISSING $CERT_EXPORT_HELPER"
fi
if [ ! -r $KAFKA_INCLUDE_SCRIPT ]; then
    SETUP_TOOL_MISSING="$SETUP_TOOL_MISSING $KAFKA_INCLUDE_SCRIPT"
fi
if [ ! -r $KAFKA_KAFKA_IDGEN_HELPER ]; then
    SETUP_TOOL_MISSING="$SETUP_TOOL_MISSING $KAFKA_KAFKA_IDGEN_HELPER"
fi
if [ -n "$SETUP_TOOL_MISSING" ]; then
    echo "ERROR: some file(s) missing: $SETUP_TOOL_MISSING"
    echo "$SETUP_TOOL_RPM not installed?"
    exit 1
fi

IAM_TOOL_MISSING=""
SETUP_PWD_HELPER="/opt/autopilot/wso2is/tools/wso2admin-setuserpassword"
if [ ! -x $SETUP_PWD_HELPER ]; then
    IAM_TOOL_MISSING="$IAM_TOOL_MISSING $SETUP_PWD_HELPER"
fi
if [ -n "$IAM_TOOL_MISSING" ]; then
    echo "ERROR: some file(s) missing: $IAM_TOOL_MISSING"
    echo "$IAM_TOOL_RPM not installed?"
    exit 1
fi

KAFKA_TOPIC_HELPER=/opt/autopilot/kafka/bin/kafka-topics.sh
if [ ! -x $KAFKA_TOPIC_HELPER ]; then
    echo "ERROR: $KAFKA_TOPIC_HELPER is not there or not executable"
    exit 1
fi

# ---------------------------------------------------------------------- #
. $INCLUDE_COMMON
ADMIN_CLI_SETUP=/opt/autopilot/admin/setup_admin_tools.sh
# make this optional (compatibility with 5.3.x):
if [ -x $ADMIN_CLI_SETUP ]; then
    $ADMIN_CLI_SETUP
fi

INIT_ORIGIN="/opt/autopilot/connectit/scripts"
INIT_TARGET="/etc/init.d"
SERVICE_NAME_PREFIX="connect"

C_INCIDENT=false
C_CONFIG=false
C_CHANGE=false


CONNECT_REST_KEYSTORE="/opt/autopilot/connectit/conf/keystore.jks"

WSO2USER=admin
WSO2PWD=$WSO2USER
CONNECT_SP_NAME="ConnectIT"
CONNECT_USER_NAME="connectituser"
CONNECT_USER_PASS=""
SKIP_PREPARE=""

FILE_OWNER=graphit
FILE_GROUP=graphit
CONNECT_CERT_ALIAS="connectit"
ALT_JAVA_HOME=""

# improve hard coded topic list
TOPIC_LIST="Incident Change Config IncidentSync ChangeSync ConfigSync IncidentCallback ChangeCallback ConfigCallback"
TOPIC_REPL_FACTOR=1
TOPIC_PARTITIONS=10

# ---------------------------------------------------------------------- #
SHOW_HELP=""
ARG_SHIFT=0
while getopts "hu:p:sj:" OPT; do
    case $OPT in
        h) SHOW_HELP="y"; ARG_SHIFT=$(( $ARG_SHIFT + 1 ));;
        u) WSO2USER=$OPTARG; ARG_SHIFT=$(( $ARG_SHIFT + 2 ));;
        p) WSO2PWD=$OPTARG; ARG_SHIFT=$(( $ARG_SHIFT + 2 ));;
        s) SKIP_PREPARE="y"; ARG_SHIFT=$(( $ARG_SHIFT + 1 ));;
        j) ALT_JAVA_HOME=$OPTARG; ARG_SHIFT=$(( $ARG_SHIFT + 2 ));;
    esac
done
shift $ARG_SHIFT

if [ -n "$SHOW_HELP" ]; then
    cat <<EOF

Usage: $0 [-h] \\
  [-u <wso2adminUser>] [-p <wso2adminPassword>] [-j <non-default-java-home>]

-h              show this help
-u <username>   admin credentials for IAM server
-p <password>   admin credentials for IAM server
-s              skip prepare step (for repated runs)
-j <javahome>   alternative javahome (def: /usr/java/latest)
EOF

    exit 0
fi

check_rpm autopilot-connect-meta
check_file /opt/autopilot/conf/certs/wso2carbon.cert "Forgot to copy from IAM node?"
check_file /opt/autopilot/conf/certs/graphit-server.cert "Forgot to copy from database node?"

message "ConnectIT Setup started"

SETUP_TYPE=CONNECT
CMD="$PREPARE_SCRIPT -t $SETUP_TYPE"
if [ -n "$ALT_JAVA_HOME" ]; then
    CMD="$CMD -j $ALT_JAVA_HOME"
fi
if [ -n "$SKIP_PREPARE" ]; then
    if [ ! -r $SETUP_RESPONSE_FILE ]; then
        echo "ERROR: you asked for -s (skip-prepare-setup) but $SETUP_RESPONSE_FILE is not there"
        echo "pls. run: $CMD"
        exit 1
    fi
    message "skip $PREPARE_SCRIPT"
else
    message "run $PREPARE_SCRIPT"
    # ------------- interactive part -----------------------
    $CMD
    RC=$?
    if [ $RC != 0 ]; then
        message "ERROR: failed to run: $CMD"
        exit 1
    fi
fi

# ---------------------------------------------------------------------- #
# automatic part
# ---------------------------------------------------------------------- #

CONFIGURATOR_ARGS=""
if [ -r $SETUP_RESPONSE_FILE ]; then
    CONFIGURATOR_ARGS=" --silent --force --settings-file $SETUP_RESPONSE_FILE"
fi

# first run of config (basically to have WSO2 connections correct)
CMD="$JAVA_BIN -cp $AP_CONFIG_JAR $AP_CFG_CONFIGURATOR_CLASS --component ConnectIT-Framework $CONFIGURATOR_ARGS"
echo "$CMD" | tee -a $SETUP_LOG
$CMD >> $SETUP_LOG 2>&1
RC=$?
if [ $RC != 0 ]; then
    message "ERROR: last command failed. check $SETUP_LOG for details"
    exit 1
else
    message "... done"
fi

WSO2HOSTNAME=$($JAVA_BIN -cp $AP_CONFIG_JAR $AP_CFG_READER_CLASS --parameter WSO2.hostname)
WSO2PORT=$($JAVA_BIN -cp $AP_CONFIG_JAR $AP_CFG_READER_CLASS --parameter WSO2.port)

CMD="$RETRIEVE_OAUTH_HELPER --url https://$WSO2HOSTNAME:$WSO2PORT --username $WSO2USER --password $WSO2PWD $CONNECT_SP_NAME"
# do not log (passwords in output)
#echo "$CMD" | tee -a $SETUP_LOG
$CMD >> $SETUP_LOG 2>&1
RC=$?
if [ $RC != 0 ]; then
    message "ERROR: last command failed. check $SETUP_LOG for details"
    exit 1
else
    message "... done"
fi

message "change password for user $CONNECT_USER_NAME"
CMD="$SETUP_PWD_HELPER --url https://$WSO2HOSTNAME:$WSO2PORT --username $WSO2USER --password $WSO2PWD --account-name $CONNECT_USER_NAME --generate-password"
# do not log (passwords in output)
#echo "$CMD" | tee -a $SETUP_LOG
CONNECT_USER_PASS=$($CMD 2> $SETUP_LOG)
RC=$?
if [ $RC != 0 ]; then
    message "ERROR: last command failed. check $SETUP_LOG for details"
    message "potential reason: $CONNECT_SP_NAME data is not loaded into WSO2IS, yet"
    exit 1
else
    CMD="$JAVA_BIN -cp $AP_CONFIG_JAR $AP_CFG_CONFIGURATOR_CLASS --component ConnectIT-Framework --silent --force --settings \"ConnectIT.wso2User:$CONNECT_USER_NAME\" --settings \"ConnectIT.wso2Pass:$CONNECT_USER_PASS\""
    # do not log (passwords in output)
    #echo "$CMD" | tee -a $SETUP_LOG
    $CMD >> $SETUP_LOG 2>&1
    RC=$?
    if [ $RC != 0 ]; then
        message "ERROR: last command failed. check $SETUP_LOG for details"
        exit 1
    else
        message "... done"
    fi
fi


# take care of SSL setup for REST API
CONNECT_REST_KEYSTORE_PATH=$(dirname $CONNECT_REST_KEYSTORE)
if [ ! -d $CONNECT_REST_KEYSTORE_PATH ]; then
    mkdir -p $CONNECT_REST_KEYSTORE_PATH
fi
# TODO: add force mode here
CREATE_NEW_CERT=""
if [ -e $CONNECT_REST_KEYSTORE ]; then
    message "INFO: keystore file ( $CONNECT_REST_KEYSTORE) already exists. skip creation"
else
    CREATE_NEW_CERT="y"
fi
if [ -n "$CREATE_NEW_CERT" ]; then
    message "create $CONNECT_REST_KEYSTORE"
    CONNECT_HOSTNAME=$($JAVA_BIN -cp $AP_CONFIG_JAR $AP_CFG_READER_CLASS --parameter ConnectIT.hostname)
    CONNECT_KEYSTORE_PWD=$(perl -le 'print map { ("a".."z","A".."Z",0..9)[rand 62] } 1..12')
    CMD="$CERT_CREATION_HELPER -H $CONNECT_HOSTNAME -s $CONNECT_REST_KEYSTORE -p $CONNECT_KEYSTORE_PWD -A $CONNECT_CERT_ALIAS -k $CONNECT_KEYSTORE_PWD -U $FILE_OWNER -G $FILE_GROUP"
    # do not log (passwords in output)
    #echo "$CMD" | tee -a $SETUP_LOG
    $CMD >> $SETUP_LOG 2>&1
    RC=$?
    if [ $RC != 0 ]; then
        message "ERROR: last command failed. check $SETUP_LOG for details"
        exit 1
    else
        message "... done"
    fi
    message "export SSL cert"
    CMD="$CERT_EXPORT_HELPER -s $CONNECT_REST_KEYSTORE -p $CONNECT_KEYSTORE_PWD -A $CONNECT_CERT_ALIAS  -U $FILE_OWNER -G $FILE_GROUP"
    # do not log (passwords in output)
    #echo "$CMD" | tee -a $SETUP_LOG
    $CMD >> $SETUP_LOG 2>&1
    RC=$?
    if [ $RC != 0 ]; then
        message "ERROR: last command failed. check $SETUP_LOG for details"
        exit 1
    else
        message "... done"
    fi
    # reconfigure ConnectIT to set SSL cert stuff correctly (2nd run)
    message "reconfigure ConnectIT"
    >$CONNECT_CFG_RESPONSE_FILE
    echo "ConnectIT.REST-API.Protocol: HTTPS" >> $CONNECT_CFG_RESPONSE_FILE
    echo "ConnectIT.REST-API.KeyStore.Path: $CONNECT_REST_KEYSTORE" >> $CONNECT_CFG_RESPONSE_FILE
    echo "ConnectIT.REST-API.KeyStore.StorePass: $CONNECT_KEYSTORE_PWD" >> $CONNECT_CFG_RESPONSE_FILE
    echo "ConnectIT.REST-API.KeyStore.KeyPass: $CONNECT_KEYSTORE_PWD" >> $CONNECT_CFG_RESPONSE_FILE
    CONFIGURATOR_ARGS=" --silent --force --settings-file $CONNECT_CFG_RESPONSE_FILE"
    # 2nd run of config
    CMD="$JAVA_BIN -cp $AP_CONFIG_JAR $AP_CFG_CONFIGURATOR_CLASS --component ConnectIT-Framework $CONFIGURATOR_ARGS"
    echo "$CMD" | tee -a $SETUP_LOG
    $CMD >> $SETUP_LOG 2>&1
    RC=$?
    if [ $RC != 0 ]; then
        message "ERROR: last command failed. check $SETUP_LOG for details"
        exit 1
    else
        message "... done"
    fi
else
    message "INFO: make sure that keystore password is correctly set and clients are provided with the correct cert (you ignore this if you are not going to use the Connect REST API)"
fi

# take care of Kafka
message "update kafka server setup"
. $KAFKA_INCLUDE_SCRIPT
. $KAFKA_IDGEN_HELPER
CURRENT_BROKER_ID=$(kafka_get_current_broker_id)
if [ $CURRENT_BROKER_ID != "0" ]; then
    BROKER_ID=$CURRENT_BROKER_ID
    message "re-use existing Kafka broker id: $BROKER_ID"
else
    BROKER_ID=$(generate_int_id)
    message "new Kafka broker id generated: $BROKER_ID"
fi
CMD="$JAVA_BIN -cp $AP_CONFIG_JAR $AP_CFG_CONFIGURATOR_CLASS --component kafka --silent --settings Kafka.broker-id:$BROKER_ID"
echo "$CMD" | tee -a $SETUP_LOG
$CMD >> $SETUP_LOG 2>&1
RC=$?
if [ $RC != 0 ]; then
    message "ERROR: last command failed. check $SETUP_LOG for details"
    exit 1
else
    message "... done"
fi
message "start kafka server"
if kafka_is_running; then
    kafka_restart
else
    kafka_start
fi
# create missing topics in Kafka
ZOOKEEPER_CONNECT_STRING=$($JAVA_BIN -cp $AP_CONFIG_JAR $AP_CFG_READER_CLASS --parameter Zookeeper.connectstring)
EXISTING_TOPIC_LIST=$($KAFKA_TOPIC_HELPER --zookeeper $ZOOKEEPER_CONNECT_STRING --describe | perl -lane 'print $1 if (/^Topic:(\S+)/)')
for TOPIC in $TOPIC_LIST; do
    FOUND=""
    for EXISTING_TOPIC in $EXISTING_TOPIC_LIST; do
        if [ "$TOPIC" == "$EXISTING_TOPIC" ]; then
            FOUND="Y"
            break
        fi
    done
    if [ -z "$FOUND" ]; then
        CMD="$KAFKA_TOPIC_HELPER --zookeeper $ZOOKEEPER_CONNECT_STRING --topic $TOPIC --create --partitions $TOPIC_PARTITIONS --replication-factor $TOPIC_REPL_FACTOR"
        echo "$CMD" | tee -a $SETUP_LOG
        $CMD >> $SETUP_LOG 2>&1
        RC=$?
        if [ $RC != 0 ]; then
            message "ERROR: last command failed. check $SETUP_LOG for details"
            exit 1
        fi
    fi
done

# ---------------------------------------------------------------------- #
# 2nd interactive part. (factor out?)
# ---------------------------------------------------------------------- #
COMPONENT_ANSWER=""
        echo "All Components will be installed"
        C_INCIDENT=incident
        C_CONFIG=config
        C_CHANGE=change
        COMPONENTS="incident config change"


PROCESSES_ANSWER=""
        echo "All Processes will be installed"
        PROCESSES="consumer backsync rest-api"
        P_CONSUMER=consumer
        P_BACKSYNC=backsync
        P_RESTAPI=rest-api
 
COPY_SCRIPTS=""
for pro in $PROCESSES;
do
    for comp in $COMPONENTS;
    do
        COPY_SCRIPTS="$comp-$pro $COPY_SCRIPTS";
        #echo "$comp-$pro";
    done
done

# generate init scripts

export INIT_ORIGIN
[ ! -d $INIT_ORIGIN ] && mkdir -p $INIT_ORIGIN

$INIT_SCRIPT_BUILDER "$COPY_SCRIPTS"

message "register startup hooks and activate services"
for script in $COPY_SCRIPTS;
do
    SERVICE_NAME="$SERVICE_NAME_PREFIX-$script"
    CMD="cp $INIT_ORIGIN/$script $INIT_TARGET/$SERVICE_NAME"
    echo "$CMD" | tee -a $SETUP_LOG
    $CMD >> $SETUP_LOG 2>&1
    #
    CMD="chkconfig --add $SERVICE_NAME"
    echo "$CMD" | tee -a $SETUP_LOG
    $CMD >> $SETUP_LOG 2>&1
    RC=$?
    if [ $RC != 0 ]; then
        message "ERROR: last command failed. check $SETUP_LOG for details"
        exit 1
    fi
    #
    CMD="chkconfig --level 35 $SERVICE_NAME on"
    echo "$CMD" | tee -a $SETUP_LOG
    $CMD >> $SETUP_LOG 2>&1
    #
    CMD="service $SERVICE_NAME restart"
    echo "$CMD" | tee -a $SETUP_LOG
    $CMD >> $SETUP_LOG 2>&1
    RC=$?
    if [ $RC != 0 ]; then
        message "ERROR: (re)start of $SERVICE_NAME failed. check $SETUP_LOG for details"
        # continue anyway
    fi
done

echo "done"

