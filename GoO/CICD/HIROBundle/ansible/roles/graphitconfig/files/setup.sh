#!/bin/bash

THIS_DIR=$(dirname $0)
INITIAL_DATA_DIR=$THIS_DIR/initial_data
MARS_NODE_FILE_TPL="$INITIAL_DATA_DIR/ogit+Automation+MARSNode/DefaultIssueNode.json.tpl"
PLUGIN_CFG_FILE=/opt/autopilot/conf/plugin-connectit.yaml

SETUP_COMMON=/opt/autopilot/setup/bash_include/_setup_common.sh
if [ ! -r $SETUP_COMMON ]; then
    echo "ERROR: $SETUP_COMMON does not exist or is not readable"
    exit 1
fi
. $SETUP_COMMON

INITIAL_LOAD_TOOL=/opt/autopilot/graphit/initial_data/load_initial_data.sh
if [ ! -x $INITIAL_LOAD_TOOL ]; then
    echo "ERROR: loader tool $INITIAL_LOAD_TOOL not found or not executable"
    exit 1
fi

# ----------------------------------------------------------------------- #
ORGANIZATION=$($JAVA_BIN -cp $AP_CONFIG_JAR $AP_CFG_READER_CLASS --parameter General.organization)
INSTANCE_NAME=$($JAVA_BIN -cp $AP_CONFIG_JAR $AP_CFG_READER_CLASS --parameter General.instance-name)
ORIG_ORGANIZATION=$ORGANIZATION
ORGANIZATION=$($JAVA_BIN -cp $AP_CONFIG_JAR $AP_CFG_STDSTRING_CLASS --replacement ":=_" "$ORGANIZATION")
echo "organization after normalization: $ORGANIZATION"
INSTANCE_NAME=$($JAVA_BIN -cp $AP_CONFIG_JAR $AP_CFG_STDSTRING_CLASS --replacement ":=_" "$INSTANCE_NAME")
echo "instance-name after normalization: $INSTANCE_NAME"

# produce MARS node def
MARS_NODE_ID="$ORGANIZATION:$INSTANCE_NAME:Machine:DefaultIssueNode"
MARS_NODE_FILE="$INITIAL_DATA_DIR/ogit+Automation+MARSNode/$MARS_NODE_ID.json"
cat $MARS_NODE_FILE_TPL | sed "s=__ORGANIZATION__=$ORGANIZATION=g" | sed "s=__INSTANCE_NAME__=$INSTANCE_NAME=g" > $MARS_NODE_FILE
# load file
$INITIAL_LOAD_TOOL -d $INITIAL_DATA_DIR

# enable plugin-events

echo ""
echo "==========="
echo ""

    echo "MARS-Node creation will be enabled"
    CMD="$JAVA_BIN -cp $AP_CONFIG_JAR $AP_CFG_CONFIGURATOR_CLASS --component ConnectIT-PluginConfig --silent --settings ConnectIT.enableMARS:true"

echo "$CMD" | tee -a $SETUP_LOG
$CMD >> $SETUP_LOG 2>&1

# update config file
touch $PLUGIN_CFG_FILE
$JAVA_BIN -cp $AP_CONFIG_JAR $AP_CFG_YAML_WRITER_CLASS -t $PLUGIN_CFG_FILE -k ticket/create/defaultNodeID -v "$MARS_NODE_ID"
$JAVA_BIN -cp $AP_CONFIG_JAR $AP_CFG_YAML_WRITER_CLASS -t $PLUGIN_CFG_FILE -k event/create/defaultNodeID -v "$MARS_NODE_ID"


$JAVA_BIN -cp $AP_CONFIG_JAR $AP_CFG_YAML_WRITER_CLASS -t $PLUGIN_CFG_FILE -k mars/create/defaultOwner -v "$ORIG_ORGANIZATION"

