#!/bin/bash
if [ $# -ne 2 ]
then
echo
echo "Usage: ./batchAnsible.sh  IPAddress chkLinkErr" 
echo
exit
fi

ipaddr=$1
echo $2 > /tmp/tmp1.out
sed 's/:/\n/g' /tmp/tmp1.out > /tmp/tmp.out

cat /tmp/tmp.out|while read line
do
hdsk=`echo $line |awk '{print $1}'`
echo  lspath -l ${hdsk} >> tmp.file
echo cd /usr/D*/bin >> tmp.file
echo "./dlnkmgr view -path |grep ${hdsk}" >> tmp.file
done

>modelt.exp
echo "#!/bin/expect" >> modelt.exp

# for first login servers

echo spawn ssh  ansible@${ipaddr} >> modelt.exp
echo match_max 100000 >> modelt.exp
echo sleep 8 >> modelt.exp
echo expect "*" >> modelt.exp
#echo sleep 2 >> modelt.exp
#echo 'expect "*"' >> modelt.exp
echo send "oem_setup_env\r" >> modelt.exp
echo sleep 5 >> modelt.exp

cat tmp.file | while read commd 
do
echo expect "*" >> modelt.exp
echo send  \"${commd}\\r\" >> modelt.exp
echo sleep 6  >> modelt.exp
done
chmod 755 modelt.exp

echo expect "*" >> modelt.exp
echo send  "exit\r" >>modelt.exp
echo sleep 6 >>modelt.exp
echo expect "*"  >> modelt.exp
echo send  "exit\r" >>modelt.exp
echo sleep 4 >> modelt.exp
echo expect eof >> modelt.exp
#cat modelt.exp
./modelt.exp  > /tmp/result.out 2>&1
cat /tmp/result.out | grep hdisk
