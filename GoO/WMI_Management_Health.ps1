function Get-NetworkStatistics
{
	[OutputType('System.Management.Automation.PSObject')]
	[CmdletBinding(DefaultParameterSetName='name')]
	
	param(
		[Parameter(Position=0,ValueFromPipeline=$true,ParameterSetName='port')]
		$Port='*',
		
		[Parameter(Position=0,ValueFromPipeline=$true,ParameterSetName='name')]
		[System.String]$ProcessName='*',
		
		[Parameter(Position=0,ValueFromPipeline=$true,ParameterSetName='address')]
		[System.String]$Address='*',		
		
		[Parameter()]
		[ValidateSet('*','tcp','udp')]
		[System.String]$Protocol='*',

		[Parameter()]
		[ValidateSet('*','Closed','CloseWait','Closing','DeleteTcb','Established','FinWait1',`
		'FinWait2','LastAck','Listen','SynReceived','SynSent','TimeWait','Unknown')]
		[System.String]$State='*'
		
	)
    
	begin
	{
		$properties = 'Protocol','LocalAddress','LocalPort'
    		$properties += 'RemoteAddress','RemotePort','State','ProcessName','PID'
	}
	
	process
	{
	    netstat -ano | Select-String -Pattern '\s+(TCP|UDP)' | ForEach-Object {

	        $item = $_.line.split(' ',[System.StringSplitOptions]::RemoveEmptyEntries)

	        if($item[1] -notmatch '^\[::')
	        {           
	            if (($la = $item[1] -as [ipaddress]).AddressFamily -eq 'InterNetworkV6')
	            {
	               $localAddress = $la.IPAddressToString
	               $localPort = $item[1].split('\]:')[-1]
	            }
	            else
	            {
	                $localAddress = $item[1].split(':')[0]
	                $localPort = $item[1].split(':')[-1]
	            } 

	            if (($ra = $item[2] -as [ipaddress]).AddressFamily -eq 'InterNetworkV6')
	            {
	               $remoteAddress = $ra.IPAddressToString
	               $remotePort = $item[2].split('\]:')[-1]
	            }
	            else
	            {
	               $remoteAddress = $item[2].split(':')[0]
	               $remotePort = $item[2].split(':')[-1]
	            } 
				
				$procId = $item[-1]
				$ProcName = (Get-Process -Id $item[-1] -ErrorAction SilentlyContinue).Name
				$proto = $item[0]
				$status = if($item[0] -eq 'tcp') {$item[3]} else {$null}				
				
				$pso = New-Object -TypeName PSObject -Property @{
					PID = $procId
					ProcessName = $ProcName
					Protocol = $proto
					LocalAddress = $localAddress
					LocalPort = $localPort
					RemoteAddress =$remoteAddress
					RemotePort = $remotePort
					State = $status
				} | Select-Object -Property $properties								


				if($PSCmdlet.ParameterSetName -eq 'port')
				{
					if($pso.RemotePort -like $Port -or $pso.LocalPort -like $Port)
					{
					    if($pso.Protocol -like $Protocol -and $pso.State -like $State)
						{
							$pso
						}
					}
				}

				if($PSCmdlet.ParameterSetName -eq 'address')
				{
					if($pso.RemoteAddress -like $Address -or $pso.LocalAddress -like $Address)
					{
					    if($pso.Protocol -like $Protocol -and $pso.State -like $State)
						{
							$pso
						}
					}
				}
				
				if($PSCmdlet.ParameterSetName -eq 'name')
				{		
					if($pso.ProcessName -like $ProcessName)
					{
						if($pso.Protocol -like $Protocol -and $pso.State -like $State)
						{
					   		$pso
						}
					}
				}
	        }
	    }
	}
}
$ErrorActionPreference = 'Continue'
try{ 
$batteryInfo = Get-WmiObject win32_battery | select EstimatedChargeRemaining,EstimatedRunTime,Status
if (!$batteryInfo){
$batteryInfo = New-Object -TypeName PSObject
Add-Member -InputObject $batteryInfo -MemberType NoteProperty `
  -Name EstimatedChargeRemaining -Value ""
Add-Member -InputObject $batteryInfo -MemberType NoteProperty `
  -Name EstimatedRunTime -Value ""
Add-Member -InputObject $batteryInfo -MemberType NoteProperty `
  -Name Status -Value ""
}
}
catch{
$batteryInfo = New-Object -TypeName PSObject
Add-Member -InputObject $batteryInfo -MemberType NoteProperty `
  -Name EstimatedChargeRemaining -Value ""
Add-Member -InputObject $batteryInfo -MemberType NoteProperty `
  -Name EstimatedRunTime -Value ""
Add-Member -InputObject $batteryInfo -MemberType NoteProperty `
  -Name Status -Value ""
}
try{
$hardDriveInfo= Get-WmiObject Win32_DiskDrive | select Status

}
catch{
$hardDriveInfo = New-Object -TypeName PSObject
}
try{
$openNetworkConnection= Get-NetworkStatistics |select pid,localaddress,protocol,remoteaddress,processname,state

}
catch{
$openNetworkConnection = New-Object -TypeName PSObject
}
try{
$massStorageDeviceInfo=Get-WmiObject Win32_LogicalDisk | select DeviceId,FreeSpace
foreach ($massStorageDevice in $massStorageDeviceInfo) {
$massStorageDevice.FreeSpace=""+ [math]::round($massStorageDevice.FreeSpace /1Gb, 3) +"GB"
}
}
catch{
$massStorageDeviceInfo = New-Object -TypeName PSObject
}
try{
$printerInfo=Get-WmiObject Win32_Printer | select Name,PrinterStatus 
foreach ($printer in $printerInfo) {
   switch($printer.PrinterStatus){
   1{$printer.PrinterStatus="Other"}
   2{$printer.PrinterStatus="Unknown"}
   3{$printer.PrinterStatus="Idle"}
   4{$printer.PrinterStatus="Printing"}
   5{$printer.PrinterStatus="Warming Up"}
   6{$printer.PrinterStatus="Stopped Printing"}
   7{$printer.PrinterStatus="Offline"}
 }
}
}
catch{
$printerInfo = New-Object -TypeName PSObject
}
try{
$displayAdaptorInfo=Get-WmiObject Win32_VideoController | select Status
}
catch{
$displayAdaptorInfo = New-Object -TypeName PSObject
}
try{
$netAdapterInfo=Get-WmiObject -class Win32_NetworkAdapterConfiguration -Filter "IpEnabled = 'True'" | select Description,IpEnabled
}
catch{
$netAdapterInfo = New-Object -TypeName PSObject
}
try{
$systemDriverInfo=Get-WMIObject -class Win32_SystemDriver | select DisplayName, Status
}
catch{
$systemDriverInfo = New-Object -TypeName PSObject
}
try{
$windowsInfo= Get-WmiObject win32_operatingsystem | select lastbootuptime,FreeVirtualMemory,FreePhysicalMemory 
#$windowsInfo.FreeVirtualMemory="" +[math]::round($windowsInfo.FreeVirtualMemory /1GB, 3) +"GB"
#$windowsInfo.FreePhysicalMemory=""+ [math]::round($windowsInfo.FreePhysicalMemory /1GB, 3) +"GB"
$os= Get-WmiObject win32_operatingsystem
$uptime = (Get-Date) - ($os.ConvertToDateTime($os.lastbootuptime))
   $Display = ""+ $Uptime.Days + " days, " + $Uptime.Hours + " hours, " + $Uptime.Minutes + " minutes" 
   $windowsInfo.lastbootuptime= $Display
   
}
catch{
$windowsInfo = New-Object -TypeName PSObject
}

try{
$devicePwrState=Get-WMIObject -class Win32_ComputerSystem | select PowerState
}
catch{
$devicePwrState = New-Object -TypeName PSObject
}


$healthInfo = New-Object -TypeName PSObject

$healthInfo | Add-Member -MemberType NoteProperty �Name 'batteryInfo' �Value $batteryInfo

$healthInfo | Add-Member -MemberType NoteProperty  �Name 'massStorageDeviceInfo' �Value $massStorageDeviceInfo

$healthInfo | Add-Member �MemberType NoteProperty �Name 'printerInfo' �Value $printerInfo

$healthInfo | Add-Member �MemberType NoteProperty �Name 'displayAdaptorInfo' �Value $displayAdaptorInfo

$healthInfo | Add-Member �MemberType NoteProperty �Name 'netAdapterInfo' �Value $netAdapterInfo

$healthInfo | Add-Member �MemberType NoteProperty �Name 'openNetworkConnection' �Value $openNetworkConnection

$healthInfo | Add-Member -MemberType NoteProperty �Name 'hardDriveInfo' �Value $hardDriveInfo

$healthInfo | Add-Member -MemberType NoteProperty �Name 'windowsInfo' �Value $windowsInfo

$healthInfo | Add-Member �MemberType NoteProperty �Name 'systemDriverInfo' �Value $systemDriverInfo

$healthInfo | Add-Member �MemberType NoteProperty �Name 'devicePwrState' �Value $devicePwrState

try{
echo ($healthInfo | ConvertTo-JSON )
}
catch{
$ErrorMessage = $_.Exception.Message
	Write-Output "Exception in conversion : "$ErrorMessage
}