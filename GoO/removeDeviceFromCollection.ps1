﻿[CmdletBinding()]
Param(
    [Parameter(Mandatory=$true)]
	[string]$ResourceName,
	[Parameter(Mandatory=$false)]
    [string]$EmailAddress,
	[Parameter(Mandatory=$true)]
    [String[]]$Collections,
	[Parameter(Mandatory=$false)]
    [int]$ResourceId,
	[Parameter(Mandatory=$false)]
	[AllowEmptyString()]
	[AllowNull()]
    [string]$LogPath
	)
	
$directoryPath = "C:\EUO\"
$logtime =  Get-Date -Format "MM-dd-yyyy"
If ((-not ([string]::IsNullOrEmpty($LogPath))) -and $LogPath -ne "NoPath")
{
$directoryPath = $LogPath
}
if([IO.Directory]::Exists($directoryPath))
{
    #Do Nothing!!
}
else
{
    New-Item -ItemType directory -Path $directoryPath
}
$path = $directoryPath+"\EUOSCCMAudit_"+$logtime+".log"

foreach ($CollectionID in $Collections)
{

#*****************************common functions start here******************************************
#if you are not running the script on your SCCM Server(SMS provider),you will have to provide the SCCM server name here. Replace Localhost with your SCCM server
$SMSProvider = "localhost"
 Function Get-SiteCode
{
$wqlQuery = “SELECT * FROM SMS_ProviderLocation”
$a = Get-WmiObject -Query $wqlQuery -Namespace “root\sms” -ComputerName $SMSProvider
#write-host $a
$a | ForEach-Object {
if($_.ProviderForLocalSite)
{
$script:SiteCode = $_.SiteCode
}
}
}
Get-SiteCode
#Import the CM12 Powershell cmdlets
$SiteNamespace = ‘root\sms\site_’ + $SiteCode
if (-not (Test-Path -Path $SiteCode))
{
Write-Verbose "$(Get-Date):   CM12 module has not been imported yet, will import it now."
Import-Module ($env:SMS_ADMIN_UI_PATH.Substring(0,$env:SMS_ADMIN_UI_PATH.Length – 5) + '\ConfigurationManager.psd1') | Out-Null
}
#CM12 cmdlets need to be run from the CM12 drive
Set-Location "$($SiteCode):" | Out-Null
if (-not (Get-PSDrive -Name $SiteCode))
{
Write-Error "There was a problem loading the Configuration Manager powershell module and accessing the site's PSDrive."
exit 1
}
#***************Common functions ends here************************************************************
Remove-CMDeviceCollectionDirectMembershipRule -CollectionID $CollectionID -ResourceName $ResourceName -Force
$Query = "SELECT * FROM SMS_Collection WHERE CollectionID = '" + $CollectionId + "'"
$Coll = Get-WmiObject -Namespace $SiteNamespace -Authentication 6 -Impersonation 3 -Query $Query
$SampleString = "User {0} has removed device {1} from collection {3} ({2})  at {4}" -f $EmailAddress,$ResourceName,$CollectionId,$Coll.Name,(Get-Date).ToString("F")
add-content -Path $path -Value $SampleString -Force
echo "deleted"  
}

