#!/usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import absolute_import, division, print_function
__metaclass__ = type

ANSIBLE_METADATA = {'metadata_version': '1.1', 
                     'status': ['preview'],
                    'supported_by': 'community'}

import re

from ansible.module_utils.basic import AnsibleModule

def parse_fs(data,devname):
  for line in data.splitlines():
    if devname in line:
      sp=[]
      sp=line.split()
      fsname=sp[-1]
      return(0,fsname)
    return(1,"notFound")



def main():
  module = AnsibleModule(
    argument_spec=dict(
      devname=dict(type='str', required=True)),
      supports_check_mode=False,
    )

  devname = module.params['devname']
  df_cmd = module.get_bin_path("df", required=True) 
  rc=rc1=err=fsName=''
  rc, fs_info, err = module.run_command("%s -g %s" % (df_cmd, devname))
  rc1,fsName=parse(fs_info,devname)  

  if rc1 == 0:
    module.exit_json(changed=True, msg="File system name is: %s ." % fsName)
  else:
    module.fail_json(msg="Failed to find the related file system.")

if __name__ == '__main__':
  main()
