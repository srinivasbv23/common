[CmdletBinding()]
Param(
    [Parameter(Mandatory=$true)]
	[string]$client,
	[Parameter(Mandatory=$false)]
    [string]$EmailAddress,
	[Parameter(Mandatory=$false)]
	[AllowEmptyString()]
	[AllowNull()]
    [string]$LogPath
	)

$directoryPath = "C:\EUO\"
$logtime =  Get-Date -Format "MM-dd-yyyy"
If ((-not ([string]::IsNullOrEmpty($LogPath))) -and $LogPath -ne "NoPath")
{
$directoryPath = $LogPath
}
if([IO.Directory]::Exists($directoryPath))
{
    #Do Nothing!!
}
else
{
    New-Item -ItemType directory -Path $directoryPath
}
$path = $directoryPath+"\EUOSCCMAudit_"+$logtime+".log"

Import-Module (Join-Path $(Split-Path $env:SMS_ADMIN_UI_PATH) ConfigurationManager.psd1)
$SiteCode = Get-PSDrive -PSProvider CMSITE
echo "site code is $SiteCode"
$SMSProvider=$sitecode.SiteServer
Set-Location "$($SiteCode.Name):\"
$CN=Get-CMDevice -Name $client
$name=$CN.Name
if ($name) 
   {
	Try
	{
	   Remove-CMDevice -name $client -force 
	   echo "Device $client is successfully deleted from SCCM server."
	   $SampleString = "User {0} has removed device {1} from SCCM Server at {2}" -f $EmailAddress,$client,(Get-Date).ToString("F")
	   add-content -Path $path -Value $SampleString -Force
	}Catch
	{
		$e = $_.Exception
		$msg = $e.Message
		echo $msg
	   $SampleString = "User {0} has tried to remove device {1} from SCCM Server at {2} but failed due to {3} " -f $EmailAddress,$client,(Get-Date).ToString("F"),$msg
	   add-content -Path $path -Value $SampleString -Force
	}
   }else{
	   $SampleString = "User {0} has tried to remove device {1} from SCCM Server at {2} but device is not found in SCCM server" -f $EmailAddress,$client,(Get-Date).ToString("F")
	   add-content -Path $path -Value $SampleString -Force
	   echo "Device $client is not found in SCCM server"
   }
