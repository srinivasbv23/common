[CmdletBinding()]
Param(
    [Parameter(Mandatory=$true)]
    [string]$SiteCode,
    [Parameter(Mandatory=$true)]
    [int]$Op,
    [Parameter(Mandatory=$false)]
    [string]$CollectionId,
    [Parameter(Mandatory=$false)]
    [string]$ResourceClassName = "SMS_R_System",
    [Parameter(Mandatory=$false)]
    [int]$ResourceId,
	[Parameter(Mandatory=$false)]
    [string]$EmailAddress,
	[Parameter(Mandatory=$false)]
    [string]$ResourceName,
	[Parameter(Mandatory=$false)]
	[AllowEmptyString()]
	[AllowNull()]
    [string]$LogPath
)

$SiteNamespace = �root\sms\site_� + $SiteCode.Trim()
$directoryPath = "C:\EUO\"
$logtime =  Get-Date -Format "MM-dd-yyyy"
If ((-not ([string]::IsNullOrEmpty($LogPath))) -and $LogPath -ne "NoPath")
{
$directoryPath = $LogPath
}
if([IO.Directory]::Exists($directoryPath))
{
    #Do Nothing!!
}
else
{
    New-Item -ItemType directory -Path $directoryPath
}
$path = $directoryPath+"\EUOSCCMAudit_"+$logtime+".log"
echo $path
function GetAllSystems(){    
    #echo $SiteNamespace
    $JsonData = Get-WmiObject -Namespace $SiteNamespace -Authentication 6 -Impersonation 3 `
    -Query "SELECT * FROM SMS_R_System WHERE OperatingSystemNameandVersion LIKE '%Workstation%'" |
    Select-Object ResourceID,name,OperatingSystemNameandVersion,AMTFullVersion,AMTStatus,IPAddresses,MACAddresses,Client |
    Sort-Object name | ConvertTo-JSon
    #echo $Res

    $JsonData | Write-Output
}

function CreateCollectionObj($CollectionID, $Name, $TargetSecurityTypeID, $TargetSubName, $InstanceKey, $ContainerNodeID){
    $DeployIntent = 1
    if (-not ($TargetSubName -eq $null) -and $TargetSubName -eq "Remove") { $DeployIntent = 2 }

    $ContainerId = 0
    if (-not ($ContainerNodeID -eq $null)) { $ContainerId = $ContainerNodeID }

    $CollObj = New-Object PSObject �Property @{
        'CollectionID'=$CollectionID;
        'Name'=$Name;
        'TargetSecurityTypeID'=$TargetSecurityTypeID;
        'DeploymentIntent'=$DeployIntent;
        'InstanceKey'=$InstanceKey;
        'ContainerID'=$ContainerId
        }

    return $CollObj
}

function GetCollectionsForSoftwareDeployment(){
    
    $Query = "SELECT DISTINCT c.CollectionID, c.Name, ds.TargetSecurityTypeID, ds.TargetSubName, oci.InstanceKey, ocn.ContainerNodeID FROM SMS_Collection AS c "`
            + "INNER JOIN SMS_DeploymentInfo AS ds ON c.CollectionID = ds.CollectionID "`
            + "LEFT OUTER JOIN SMS_ObjectContainerItem AS oci ON c.CollectionID = oci.InstanceKey "`
            + "LEFT OUTER JOIN SMS_ObjectContainerNode AS ocn ON oci.ContainerNodeID = ocn.ContainerNodeID "`
            + "WHERE c.CollectionType=2 AND ds.DeploymentIntent<3 ORDER BY c.Name"

#    $Query = "Select Name From SMS_Collection"
    $Colls = @()
        
    $ManBaseObjs = Get-WmiObject -Namespace $SiteNamespace -Authentication 6 -Impersonation 3 -Query $Query
    Foreach ($BaseObj in $ManBaseObjs){
        $CollObj = CreateCollectionObj $BaseObj["c"]["CollectionID"] $BaseObj["c"]["Name"] $BaseObj["ds"]["TargetSecurityTypeID"] `
        $BaseObj["ds"]["TargetSubName"] $BaseObj["oci"]["InstanceKey"] $BaseObj["ocn"]["ContainerNodeID"]
        #echo $BaseObj["c"]["CollectionID"]

        $Colls += $CollObj
    }

    $JsonData = $Colls | Sort-Object -Property Name | ConvertTo-JSon
    $JsonData | Write-Output
}

function CollectionContainsQueryRule($Collection){
    $ContainsRule = $false
    #echo $Collection["CollectionRules"]

    Foreach ($Rule in $Collection["CollectionRules"]){
        if ($Rule.__CLASS -eq "SMS_CollectionRuleDirect" -and $Rule.ResourceClassName -eq $ResourceClassName -and $Rule.ResourceID -eq $ResourceId){
            $ContainsRule = $true
            break
        }
        #echo $Rule["ResourceClassName"]
    }

    return $ContainsRule
}

function GetCollectionObj(){
        
    #$Filter = "CollectionID='" + $CollectionId + "'"
    #$ManBaseObjs = Get-WmiObject -Namespace $SiteNamespace -Authentication 6 -Impersonation 3 -Class "SMS_Collection" -Filter $Filter
    $Query = "SELECT * FROM SMS_Collection WHERE CollectionID = '" + $CollectionId + "'"
    $ManBaseObjs = Get-WmiObject -Namespace $SiteNamespace -Authentication 6 -Impersonation 3 -Query $Query
    $ManBaseObjs.Get()

    return $ManBaseObjs
    #echo $ManBaseObjs
}

function RefreshCollection($CollObj){
    $ManBaseObj = $CollObj.RequestRefresh($false)
}

function AddDirectRuleToCollection(){
    $Coll = GetCollectionObj
    $CollName = $Coll.Name

    if (-not (CollectionContainsQueryRule $Coll)){
        $NewRule = (Get-WmiObject -List -Namespace $SiteNamespace -Authentication 6 -Impersonation 3 -Class SMS_CollectionRuleDirect).CreateInstance()
        $NewRule["ResourceClassName"] = $ResourceClassName
        $NewRule["ResourceID"] = $ResourceId
        #echo $NewRule

        $ManBaseObj = $Coll.AddMembershipRule($NewRule)

        echo "Resource $ResourceId successfully added to collection $CollName. Refreshing ..."
		$SampleString = "User {0} has added device {3} ({4}) to collection {1} ({2}) at {5}" -f $EmailAddress,$CollectionId,$CollName,$ResourceName,$ResourceId,(Get-Date).ToString("F")
		add-content -Path $path -Value $SampleString -Force

 
    }
    else { 
		echo "Resource $ResourceId successfully added to collection $CollName. Refreshing ..."
		$SampleString = "User {0} has requested the collection {1} ({2}) for device {3} ({4}) at {5} but device is already part of collection" -f $EmailAddress,$CollectionId,$CollName,$ResourceName,$ResourceId,(Get-Date).ToString("f")
		add-content -Path $path -Value $SampleString -Force
	echo "Resource $ResourceId is part of the collection $CollName. Refreshing ..." }

    RefreshCollection $Coll
}

switch($Op)
{
    1 { GetAllSystems }
    2 { GetCollectionsForSoftwareDeployment }
    3 { AddDirectRuleToCollection }
}