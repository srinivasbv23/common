#!/bin/sh
rpm -iUvh http://dl.fedoraproject.org/pub/epel/epel-release-latest-6.noarch.rpm
yum -y install ansible

ssh-keygen -t rsa -b 4096 -C "ansiblehost" -N "" -f "/root/.ssh/ansibleid_rsa" -q
cd /root/.ssh/
cat ansibleid_rsa.pub >> authorized_keys

chmod 600 authorized_keys
cd /root
chmod 700 .ssh

cd /root/HIROBundle/ansible/
cp -rf * /etc/ansible/

echo [localhost] > /etc/ansible/hosts
echo localhost >> /etc/ansible/hosts

echo "Ansible Control Host Configuration has been Successful. Please refer the documentation."
