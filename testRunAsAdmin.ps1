[CmdletBinding()]
Param(
    [Parameter(Mandatory=$true)]
	[string]$drive
	)

echo $drive
$Volume=Get-WmiObject -Class win32_volume -Filter "DriveLetter='$DriveLetter'"
$Defrag = $Volume.Defrag($false)
Switch ($Defrag.ReturnValue) {
	0 { $result.output = "Defragmentation completed successfully..." }
	1 { $result.output = "Defragmentation of volume $DriveLetter  failed: Access Denied" }
	2 { $result.output = "Defragmentation of volume $DriveLetter  failed: Defragmentation is not supported for this volume" }
	3 { $result.output = "Defragmentation of volume $DriveLetter  failed: Volume dirty bit is set" }
	4 { $result.output = "Defragmentation of volume $DriveLetter  failed: Insufficient disk space" }
	5 { $result.output = "Defragmentation of volume $DriveLetter  failed: Corrupt master file table detected" }
	6 { $result.output = "Defragmentation of volume $DriveLetter  failed: The operation was cancelled" }
	7 { $result.output = "Defragmentation of volume $DriveLetter  failed: The operation was cancelled" }
	8 { $result.output = "Defragmentation of volume $DriveLetter  failed: A disk defragmentation is already in process" }
	9 { $result.output = "Defragmentation of volume $DriveLetter  failed: Unable to connect to the defragmentation engine" }
	10 { $result.output = "Defragmentation of volume $DriveLetter  failed: A defragmentation engine error occurred" }
	11 { $result.output = "Defragmentation of volume $DriveLetter failed: Unknown error" }
}                            

$Defrag | Set-Content 'C:\\Windows\\Temp\\defrag.txt'